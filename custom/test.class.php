<?php
// 要测试的类1
class Cal{
    // 计算是不是质数
    function prime($a){
        //        if($a==1) return false;
        for($i=2; $i<sqrt($a); ++$i){
            if($a%$i==0) return false;
        }
        return true;
    }
}
class TestMode extends Data {
    /** @return TestMode */
    public static function init() {
        return parent::init();
    }

    function getList($c){
        // 模拟返回getList的情况
        return array($this->db->getAll("select ? as name",$c),1);
    }

    function create($a){
        // 模拟数据库插入
        $this->db->insert("table",array("a"=>$a));
        return $this->db->insertId();
    }
}
class test extends Activity {
    /** @var  Testify */
    protected $testify;
    function __construct() {
        // 建立单元测试管理类
        $this->testify=new Testify();
    }

    protected function cal(){
        $toBeCal=new Cal();
        $calPrimeCase=new TestifyCase("计算类素数测试");
        $calPrimeCase->testCase(function($assert, $a)use($calPrimeCase, $toBeCal){
            // 注意哦，函数第一个参数必须要这个名字，代表比较的数据，后续更多的参数表示需要用的参数
            $re=$toBeCal->prime($a);
            $calPrimeCase->compare($assert[0]==$re);

        });
        $calPrimeCase->addData(array(false), array(15));
        $calPrimeCase->addData(array(true), array(17));
        $calPrimeCase->addData(array(false), array(1), "测试数字1");
        $calPrimeCase->addData(array(true), array(2), "测试最小素数");
        $this->testify->test($calPrimeCase);
    }

    protected function activity(){
        import("Custom.cms");
        $toBeActivity=new cms();
        $calPrimeCase=new TestifyCase("Activity类 cms.class.php测试示范 之 Page");
        $calPrimeCase->testCase(function($assert, $total, $page)use($calPrimeCase, $toBeActivity){
            $toBeActivity->pageTask($total, $page);
            $re= View::getLastResult();
            $page=$re['page'];
            $calPrimeCase->compare($page instanceof PageWidget);
            if(!$page instanceof PageWidget) return;
            $calPrimeCase->compare($assert[0]==$page->getTotal());
            $calPrimeCase->compare($assert[1]==$page->getCurrentPage());
        });
        $calPrimeCase->addData(array(100,5), array(3000,5), "通常情况");
        $calPrimeCase->addData(array(100,1), array(3000,0), "页码过小");
        $calPrimeCase->addData(array(100,100), array(3000,599), "页码过大");
        $calPrimeCase->addData(array(0,0), array(0,5), "没有内容");
        $calPrimeCase->addData(array(0,0), array(-100,5), "总内容为负");

        $this->testify->test($calPrimeCase);
    }

    protected function mode(){
        $toBeMode=TestMode::init();
        // 也可以采用类似jquery的方式添加测试数据
        $modeListCase=new TestifyCase("Mode类测试示范 之 getList");
        $modeListCase
            ->addData(array(array('name'=>'apple'),1), array('apple'))
            ->addData(array(array('name'=>'banana'),1), array('banana'))
            ->testCase(function($assert, $c)use($modeListCase, $toBeMode){
                list($re,$total)= $toBeMode->getList($c);
                $modeListCase->compare($assert[0]['name']==$re[0]['name']);
                $modeListCase->compare($assert[1]==$total);
            });
        $this->testify->test($modeListCase);
    }

    function indexTask(){
        $this->cal();
        $this->activity();
        $this->mode();

//        $this->testify->reportCgi();
        $suiteResults=$this->testify->report();
        View::displayAsHtml($suiteResults,"plugin/test/report.php");
    }
}