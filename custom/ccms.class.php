<?php
class ccms extends CodeCmsActivity {
    function __construct() {
        parent::__construct();
        $this->cms->setPageTitle("自动管理");

        $this->fieldAdd("id","ID",self::TYPE_PRIMARY,self::TABLE_STRING,self::FORM_HIDDEN,
            null,self::FORM_NONE,self::SORT_DEFAULT)
            ->fieldModify("id",array("sortDefault"=>false));
        $this->fieldAdd("type","事件类型",self::TYPE_VARCHAR,self::TABLE_STRING,self::FORM_TEXT,
            "type",self::FORM_TEXT,self::SORT_NONE);
        $this->fieldAdd("sub_type","子事件类型", self::TYPE_VARCHAR,self::TABLE_STRING,self::FORM_TEXT,
            "type",self::FORM_TEXT);
        $this->fieldAdd("ctype","事件类型", self::TYPE_CUSTOM,self::TABLE_CUSTOM,self::FORM_NONE)
            ->fieldModify("ctype",array("showFunction"=>function($data,$config){
            return "{$data['type']} / {$data['sub_type']}";
        }));
        $this->fieldAdd("data","包含数据",self::TYPE_TEXT,self::TABLE_TEXT,self::FORM_UEDITOR,"data");

        //  对于FOrm的select类型，如果是一位数组就正常显示，如果是二维数组，第一维当做optionGroup
        $this->fieldAdd("state","状态",self::TYPE_ENUM,self::TABLE_MAP,self::FORM_SELECT,
            "type",self::FORM_SELECT)
            ->fieldModify("state",array("map"=>array("pend"=>"等待请求","done"=>"已完成","ready"=>"等待推送","push"=>"已推送","other"=>"其他"),));

        $this->fieldAdd("create_time","创建时间",self::TYPE_TIMESTAMP,self::TABLE_DATETIME,self::FORM_DATETIME,
            "time",self::FORM_NONE,self::SORT_UNIQUE);
        $this->fieldAdd("select_time","查询时间",self::TYPE_TIME,self::TABLE_TIME,self::FORM_TIME,
            "time",self::FORM_NONE,self::SORT_UNIQUE);
        $this->fieldAdd("done_time","完成时间", self::TYPE_DATE,self::TABLE_DATE,self::FORM_DATE,
            "time",self::FORM_NONE,self::SORT_UNIQUE);
        $this->pageConfig("modify_group_name",array("type"=>"事件类型","data"=>"事件数据","time"=>"时间"));
        $this->mode=EventModule::init();

        // 外键与Mode的显示方法
//        $menuModule=MenuModule::init();
//        $this->fieldAdd("menu_id","所属分类",self::TYPE_FOREIGN,self::TABLE_MODE,self::FORM_TEXT)
//            ->fieldModify("menu_id",array(
//                "mode"=>$menuModule,
//                "showFunction"=>function($data,$config)use($menuModule){
//                    return $data['name'];
//                }
//            ));

        // 可以这么增加Control部分
//        $this->cms->controlWidget->reopen("tpl/admin/control.json");
    }
}