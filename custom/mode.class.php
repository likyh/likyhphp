<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2014/12/2
 * Time: 10:41
 */

class mode extends Activity {
    /** @var  SqlDB */
    protected $db;

    function __construct() {
        $this->db=SqlDB::init();
        SimpleSession::init();
    }

    function indexTask(){
        View::displayAsHtml(array(),"mode/index.php");
    }

    function downloadTask($title,$code){
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment; filename=$title");
        echo $code;
    }

    function dbTask(){
        $db=$this->db->getAll("show databases;");
        $result['data']=array_map(function($v){return reset($v);},$db);
        View::displayAsJson($result);
    }

    function tableTask($db){
        $this->db->sqlExec("use $db;");
        $result['db']=$db;
        $data=$this->db->getAll("show tables;");
        $result['data']=array_map(function($v){return reset($v);},$data);
        View::displayAsJson($result);
    }

    function dbInfoTask($db,$table){
        $result['comment']=$this->db->getValue("Select TABLE_COMMENT from INFORMATION_SCHEMA.TABLES Where table_schema = '$db' AND table_name='$table'");
        View::displayAsJson($result);
    }

    function columnTask($db,$table,$tpl){
        $tpl=$tpl?:"base";
        $column=$this->db->getAll("desc $db.$table");
        $columnD=array();
        $data=array();
        foreach($column as &$v){
            $t=array();
            $t['field']=$field=strtolower($v['Field']);
            $t['type']=$type=strtolower($v['Type']);
            $t['null']=$v['Null']=="YES";
            $t['key']=strtolower($v['Key']);
            $t['default']=strtolower($v['Default']);
            $t['extra']=strtolower($v['Extra']);
            if($t['key']=='pri'){ // 主键
                $t['class']='pri';
            }elseif(strpos($type,'tinyint(1)')!==false){
                // 布尔型
                $t['class']='data';
                $t['classSub']='bool';
            }elseif(strpos($type,'int')!==false){
                if(asciiEndWith($field,"id")){
                    // 关联键(普通关系型）
                    $t['class']='idJoin';
                }else{
                    // 普通数据(数字型)
                    $t['class']='data';
                    $t['classSub']='number';
                    $t['classExtra']='int';
                    $t['classLength']=(int)substr($type,4,-1);
                }
            }elseif(strpos($t['default'],'current_timestamp')!==false || $field=='create_time'){
                // 时间戳
                $t['class']='time';
            }elseif($type=='timestamp'|| $type=='date'||
                $type=='time'|| $type=='datetime'){
                // 普通时间
                $t['class']='data';
                $t['classSub']='time';
            }elseif(strpos($type,'decimal')!==false||
                $type=='double'|| $type=='float'){
                // 实数(数字型)
                $t['class']='data';
                $t['classSub']='number';
                $t['classExtra']=substr($type,0,strpos($type,"("));
                $t['classLength']=(int)substr($type,strpos($type,"(")+1,-1);
            }elseif($type=='text'){
                // 大段文章
                $t['class']='data';
                $t['classSub']='text';
            }elseif(strpos($type,'varchar')!==false){
                if(asciiEndWith($field,"_ids")){
                    // 一对多关联键
                    $t['class']='idsJoin';
                }elseif(asciiEndWith($field,"_url")|| $field=='picurl'){
                    // 一对多关联键
                    $t['class']='idsJoin';
                }else{
                    // 普通的字符串数据
                    $t['class']='data';
                    $t['classSub']='normal';
                }
            }elseif(strpos($type,'enum')!==false){
                // 单选类型
                $t['enum']=array_map(function($s){
                    return substr($s,1,-1);
                },explode(',',substr($type,5,-1)));
                $t['class']='data';
                $t['classSub']='enum';
            }elseif(strpos($type,'set')!==false){
                // 多选类型
                $t['set']=array_map(function($s){
                    return substr($s,1,-1);
                },explode(',',substr($type,5,-1)));
                $t['class']='data';
                $t['classSub']='set';
            }else{
                // 未知
                $t['class']='unknown';
            }
            $columnD[$field]=$v=$t;
        }
        $result['data']=$_SESSION['data']=$column;
        $result['column']=$_SESSION['column']=$columnD;
        View::displayAsJson($result);
    }
}
