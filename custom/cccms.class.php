<?php
class cccms extends CodeCms {
    function __construct() {
        if(!RUNNING_IN_DEVELOP) die("非开发模式");
        $this->cms=CmsView::init("自动管理");
    }

    protected function infoInit(){
        $this->cms->setControlFile("plugin/cms/codeCms.json");
        $this->fieldAdd("id","ID",self::TYPE_PRIMARY,self::TABLE_STRING,self::FORM_HIDDEN)
            ->fieldAdd("table_name","表名",self::TYPE_VARCHAR,self::TABLE_STRING,self::FORM_TEXT)
            ->fieldAdd("modify_group_name","类信息JSON",self::TYPE_TEXT,self::TABLE_TEXT,self::FORM_TEXTAREA)
            ->fieldAdd("column","查看字段",self::TYPE_CUSTOM,self::TABLE_CUSTOM,self::FORM_NONE);
        $columnUrl=WebRouter::init()->getAction("columnTable","id=");
        $this->fieldModify("column",array("showFunction"=>function($data,$config)use($columnUrl){
            return "<a href='{$columnUrl}{$data['id']}'>查看字段属性</a>";
        }));
        $this->mode=CodeCmsModule::init();
    }
    protected function fieldInit(){
        $this->cms->setControlFile("plugin/cms/codeCmsColumn.json");
        $this->fieldAdd("field","字段名",self::TYPE_VARCHAR,self::TABLE_STRING,self::FORM_TEXT)
            ->fieldAdd("name","字段中文名",self::TYPE_VARCHAR,self::TABLE_STRING,self::FORM_TEXT);
        $this->fieldAdd("type","字段类型 TYPE_*",self::TYPE_ENUM,self::TABLE_MAP,self::FORM_SELECT)
            ->fieldModify("type",array("map"=>array(
                self::TYPE_VARCHAR=>"字符串 VARCHAR",
                self::TYPE_PRIMARY=>"主键 PRIMARY",
                self::TYPE_CUSTOM=>"自定义类型 CUSTOM",
                self::TYPE_TEXT=>"大段文本 TEXT",
                self::TYPE_ENUM=>"集合(单选) ENUM",
                self::TYPE_TIMESTAMP=>"时间戳 TIMESTAMP",
                self::TYPE_TIME=>"时间 TIME",
                self::TYPE_DATE=>"日期 DATE",
            )));
        $this->fieldAdd("table","表格 TABLE_*",self::TYPE_ENUM,self::TABLE_MAP,self::FORM_SELECT)
            ->fieldModify("table",array("map"=>array(
                self::TABLE_NONE=>"不显示",
                self::TABLE_STRING=>"字符串",
                self::TABLE_CUSTOM=>"自定义显示",
                self::TABLE_TEXT=>"大段文本",
                self::TABLE_MAP=>"键值对",
                self::TABLE_DATETIME=>"时间戳",
                self::TABLE_TIME=>"时间",
                self::TABLE_DATE=>"日期",
            )));
        $this->fieldAdd("form","表单 MODIFY_*",self::TYPE_ENUM,self::TABLE_MAP,self::FORM_SELECT)
            ->fieldModify("form",array("map"=>array(
                self::FORM_NONE=>"无修改 (NONE)",
                self::FORM_TEXT=>"文本域 (TEXT)",
                self::FORM_HIDDEN=>"隐藏域 (HIDDEN)",
                self::FORM_TEXTAREA=>"多行文本域 (TEXTAREA)",
                self::FORM_SELECT=>"下拉框 (SELECT)",
                self::FORM_DATETIME=>"时间戳 (DATETIME)",
                self::FORM_TIME=>"时间 (TIME)",
                self::FORM_DATE=>"日期 (DATE)",
            )));
        $this->fieldAdd("modify_group","修改表单组",self::TYPE_VARCHAR,self::TABLE_STRING,self::FORM_TEXT);
        $this->fieldAdd("search","搜索 MODIFY_*",self::TYPE_ENUM,self::TABLE_MAP,self::FORM_SELECT)
            ->fieldModify("search",array("map"=>array(
                self::FORM_NONE=>"无搜索",
                self::FORM_TEXT=>"文本域",
                self::FORM_HIDDEN=>"隐藏域",
                self::FORM_TEXTAREA=>"多行文本域",
                self::FORM_SELECT=>"下拉框",
                self::FORM_DATETIME=>"时间戳",
                self::FORM_TIME=>"时间",
                self::FORM_DATE=>"日期",
            )));
        $this->fieldAdd("sort","排序方式 SORT_*",self::TYPE_ENUM,self::TABLE_STRING,self::FORM_NONE);
        $this->mode=CodeColumnModule::init();

    }
    function tableTask($condition=array(), $sortArray=array(), $page=1){
        $this->infoInit();
        $result=$this->getTableResult($condition,$sortArray,$page);
        if($result instanceof DataMessage){
            View::displayDataMessage($result);
        }else{
            $this->cms->codeTableScene($result);
        }
    }
    function modifyTask($id=null){
        $this->infoInit();
        $result=$this->getModifyResult($id);
        if($result instanceof DataMessage){
            View::displayDataMessage($result);
        }else{
            $this->cms->codeFormScene($result);
        }
    }
    function modifySubmitTask($data){
        $this->mode=CodeCmsModule::init();
        $dm=$this->mode->update($data);
        if($dm->judgeState()){
            $url=WebRouter::init()->getAction("table");
            View::displayAsTips($url,$dm->getTitle());
        }else{
            View::displayDataMessage($dm);
        }
    }
    function deleteTask($id){
        $this->mode=CodeCmsModule::init();
        $dm=$this->mode->delete($id);
        if($dm->judgeState()){
            $url=WebRouter::init()->getAction("table");
            View::displayAsTips($url,$dm->getTitle());
        }else{
            View::displayDataMessage($dm);
        }
    }

    function columnTableTask($condition=array(), $sortArray=array(), $page=1,$id=0){
        $this->fieldInit();
        $condition+=array("likyh_code_id"=>$id);
        $result=$this->getTableResult($condition,$sortArray,$page);
        if($result instanceof DataMessage){
            View::displayDataMessage($result);
        }else{
            $result['url']['modify']="columnModify";
            $result['url']['delete']="columnDelete";
            $this->cms->codeTableScene($result);
        }
    }
    function columnModifyTask($id=null){
        $this->fieldInit();
        $result=$this->getModifyResult($id);
        if($result instanceof DataMessage){
            View::displayDataMessage($result);
        }else{
            $result['url']['submitUrl']=WebRouter::init()->getAction("columnModifySubmit","code_id=".$id);
            $this->cms->codeFormScene($result);
        }
    }
    function columnModifySubmitTask($data){
        $this->mode=CodeColumnModule::init();
        $dm=$this->mode->update($data);
        if($dm->judgeState()){
            $url=WebRouter::init()->getAction("columnTable","id=".$data['code_id']);
            View::displayAsTips($url,$dm->getTitle());
        }else{
            View::displayDataMessage($dm);
        }
    }
    function columnDeleteTask($id){
        $this->mode=CodeColumnModule::init();
        $dm=$this->mode->delete($id);
        View::displayAsTips("",$dm->title,true);
    }
}