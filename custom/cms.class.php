<?php
class cms extends Activity {
    /** @var CmsView */
    protected $cms;
    public function __construct() {
        $this->cms=CmsView::init("文章管理");
        $this->cms->setPageTitle("文章管理");
        $this->cms->setUserName("可爱的依然");
        $this->cms->setControlFile("tpl/admin/control.json");
    }

    function indexTask(){
        echo "这是首页，没有的页面也回到这里";
    }

    function loginTask(){
        $this->cms->loginScene("表单提交地址");
    }

    function itemTask(){
        $this->cms->controlWidget->addItem(null,"fa-home","自己加的","cms","table");
        $this->cms->itemScene("tpl/admin/item.json");
    }

    function formTask(){
        $this->cms->setActionTitle("修改文章");
        $this->cms->formScene(array(),"tpl/admin/form.php");
    }

    function tableTask(){
        $this->cms->setActionTitle("查看文章");
        $this->cms->tableScene(array(),"tpl/admin/table.php");
    }

    function pageTask($total=1000, $page=1){
        $this->cms->setActionTitle("查看文章");
        $url=WebRouter::init()->getQuestion("total={$total}&page=#page#");
        $page=new Page($page,$url);
//        如此可以获得查询sql用的偏移量
//        echo $page->getPageSize();
//        echo $page->getOffset();
        $page->setTotal($total);
        $result['page']=$page->getWidget(4,"all");
        $this->cms->tableScene($result,"tpl/admin/page.php");
    }
}