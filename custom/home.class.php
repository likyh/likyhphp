<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2014/12/2
 * Time: 10:41
 */

class home extends Activity {
    function indexTask(){
        echo "Hello World";
    }

    function selectTask(){
        $db=SqlDB::init();
        $data=array('count(1)','student S'=>array('id','num'=>'studentNum'),'project P'=>array('*'));
        $condition=array('type'=>1,'age'=>'> =5','age '=>'<=12  ','icon'=>'like %.jpg', 'text'=>'', 'S.area'=>null, array('or','S.name','=','sg'),'`class` is not null');
        echo $db->selectSql($data,$condition,5,0,'S.age,age2',true,'group by age');
    }

    function sqlTask(){
        $db=SqlDB::init();
        $r=$db->getOne("select * from a where id=:id and name=:name",array(":id"=>1,":name"=>"sg"));
        var_dump($r);
        $r=$db->getOne("select * from a where id=? and name=?",2,"sg");
        var_dump($r);
    }

    function chineseTask(){
        echo Chinese::pinyin("普团网");
        var_dump(Chinese::xing("吕依然"));
        var_dump(Chinese::xing("邓松高筠"));
        var_dump(Chinese::xing("上官婉儿"));
        var_dump(Chinese::xing("爱新觉罗玄烨"));
        var_dump(Chinese::xing("小可爱"));
    }

    function dataTask(){
        View::displayDataMessage(new DataMessage(DataMessage::STATE_SUCCESS,"标题","具体的描述，这个参数可以省略"));
    }

    protected $className=array();
    protected function getNodesInfo(DOMNode $node){
        if ($node->hasChildNodes()){
            $subNodes = $node->childNodes;
            foreach ($subNodes as $subNode){
                if($subNode->nodeType==1&& $subNode->hasAttribute("class")){
                    $this->className=array_merge($this->className,explode(" ",$subNode->getAttribute("class")));
                }
                $this->getNodesInfo($subNode);
            }
        }
    }
    function domTask(){
        $s=Request::http_get("http://www.pzjsh.com");
//        echo($s);
        $doc = new DOMDocument();
        @$doc->loadHTML($s);
        $node=$doc->getElementsByTagName("html")->item(0);
        $this->getNodesInfo($node);
        var_dump($this->className);
    }
}
