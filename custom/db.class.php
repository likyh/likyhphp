<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2015/06/14
 * Time: 19:04
 */
class db extends Activity {
    function eventListenTask($event){
        echo "eventListen 接收到一个事件:";
        var_dump($event);
    }
    function eventListen2Task($event){
        echo "eventListen2 接收到一个事件:";
        var_dump($event);
    }
    function eventScanTask(){
        $eventManager=EventManager::init();
        $event=$eventManager->getEventById(1);
        var_dump($event);
        $event=$eventManager->getEventById(array(2,1));
        var_dump($event);
        $event=$eventManager->getEventList(array("type"=>"user"));
        var_dump($event);
        $event=$eventManager->getEventByType("user",null);
        var_dump($event);
        $eventManager->doneEvent($event);
    }
    function eventCreateTask(){
        $eventManager=EventManager::init();
        $event=$eventManager->getNewEvent("user","register",rand(100000,999999));
        var_dump($event);
        $event2=$eventManager->getNewEvent("user","register",rand(100000,999999));
        var_dump($event2);
        $eventManager->pendEvent($event);
        $eventManager->pushEvent($event2);
    }
}
