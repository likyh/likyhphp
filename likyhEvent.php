<?php
// 注意，下面这句话，除非你是框架和类库分开存放的，否则不需要使用
//ini_set("include_path", '.;F:\workspace\www\likyhPHP\\');

// 导入类库
require_once 'likyhphpLib.php';

// 配置信息
require_once 'config/run.config.php';

// 通过Cron或者其他定时调用的服务，不断地访问该文件以完成事件的处理，如 likyhEvent.php?type=*&sub_type=*

/** @var AppInfo */
$globalAppInfo=new AppInfo();

$type=isset($_GET['type'])? $_GET['type']: null;
$sub_type=isset($_GET['sub_type'])? $_GET['sub_type']: null;
$listenList=getConfig("event",null,true);

// 获取某一类里面的n个事件
$eventManager=EventManager::init();
$eventList=$eventManager->getEventList(array("type"=>$type,"sub_type"=>$sub_type,"state"=>"ready"),1);

// 依次处理每一个事件
foreach($eventList as $event){
    echo "<br/>{$event->type} {$event->sub_type}: <br/>";
    if(!$event instanceof EventHandle) continue;
    if(!isset($listenList[$event->type][$event->sub_type])) continue;
    $eventSuccess=0;
    $eventFail=0;
    $listenStringArray=explode(",",$listenList[$event->type][$event->sub_type]);
    // 依次调用每一个事件监听函数
    foreach($listenStringArray as $listenString){
        $position=strpos($listenString,"/");
        if($position===false) continue;

        // 分析事件监听函数在哪里
        $spacePath=trim(substr($listenString,0,$position));
        $action=trim(substr($listenString,$position+1));
        $className=trim(substr($spacePath,strrpos($spacePath,".")+1));
        if(import($spacePath)){
            $object=call_user_func(array($className,"init"));
            $intent=new Intent(200,null,$object,$action,array("event"=>$event));
            startIntent($intent);
        }
    }
    // TODO 记录事件推送后的运行状态，以防止一个事件被多次推送
    if(!empty($listenStringArray)){
        $eventManager->doneEvent($event);
    }
}
