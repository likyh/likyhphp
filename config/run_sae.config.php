<?php
// 语言文件
$config['language']['default']="";

// 站点名称配置
define("CONFIG_SITE_NAME",'likyhPHP');

// 运行的环境是不是调试环境
define("RUNNING_IN_DEVELOP",true);

// likyhPHP运行配置
$config['site']= array(
    // 默认方法，此概念类似首页
    'defaultPage'=>'home',
    // 类不存在时调用的方法
    'defaultAction'=>'index',
    // 运行的位置，这一个字段开头不要加"/"，如果是网站根目录那就为空
    'commonPath'=>'likyhPHP',
    // 系统结构缓存时间,0表示不缓存
    'systemCache'=>0,
);

/**
 * @param $set
 * @return CacheInterface
 */
$config['cache']['getCache']=function($set){
    return new FileCache($set);
};

//数据库配置
$config['mysql']= array(
    'host'=>SAE_MYSQL_HOST_M,
    'port'=>SAE_MYSQL_PORT,
    'username'=>SAE_MYSQL_USER,
    'password'=>SAE_MYSQL_PASS,
    'dbname'=>SAE_MYSQL_DB,
);

// 密码配置,混淆码
$config['password']['auth'] = "AlRemainGoWithUs";

//上传配置
// 定义存在的根目录
$config['uploader']['rootPath'] = "saeStorage://upload/";
// 缓存路径
$config['uploader']['cachePath'] = SAE_TMP_PATH;
// 保存的路径
$config['uploader']['savePath'] = "upload/";
// 大小限制，单位kb
$config['uploader']['maxSize'] = 8000;
// 允许的文件类型
$config['uploader']['defaultFile'] = "doc,docx,xls,xlsx,ppt,pptx,txt,pdf,md,rar,zip,7z,mp3,gif,png,jpg,jpeg,bmp";
$config['uploader']['imageFile'] = "gif,png,jpg,jpeg,bmp";
$config['uploader']['documentFile'] = "doc,docx,xls,xlsx,ppt,pptx,txt,pdf,md,xml";
$config['uploader']['videoFile'] = "flv,swf,mkv,avi,rm,rmvb,mpeg,mpg";
$config['uploader']['musicFile'] = "ogg,ogv,mov,wmv,mp4,webm,mp3,wav,mid";
$config['uploader']['compressFile'] = "rar,zip,tar,gz,7z,bz2,cab,iso";

// redis配置
$config['redis']=array(
    'dbname'=>"eswcGYLHClgABroJarjx",
    'host'=>"redis.duapp.com",
    'port'=>"80",
    'user'=>"HNtF8PpKdoLMQTDurEuZUVTE",
    'pwd'=>"bzNmftP2pk27aeWsARgCG8Nd4lUznuug",
);
