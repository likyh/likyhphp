<?php
/**
 * 定义系统函数
 * @author linyh
 */
function __autoload($className){
    $re=import($className);
    if(!$re) trigger_error("自动导入失败:{$className} 未找到",E_USER_WARNING);
    return $re;
}

/**
 * 导入组件
 * @author linyh
 * @param $className
 * @return bool 是否成功导入
 */
function import($className){
    static $systemClass;
    static $ifAllClass=false;
    static $cacheClass;
    if(empty($systemClass)) $systemClass=getConfig("systemClass");

    // 分析是否为简写
    if(strstr($className,'.')===false){
        if(!$ifAllClass&& !isset($systemClass[$className])){
            $systemClass=getAppInfo()->getSystemCache()->cache('systemClass',"updateClass",getConfig("site","systemCache"));
            $ifAllClass=true;
        }
        if(isset($systemClass[$className])){
            $className=$systemClass[$className];
        }else{
            return false;
        }
    }
    // 检查是否导入过
    if(isset($cacheClass[$className])){
        return $cacheClass[$className];
    }

    // 检查是否符合要求，并将点号替换为/
    $componentPath=explodeDeal(DIRECTORY_SEPARATOR,explode(".",$className),function($v){
        return validateVar($v)?$v:'.';
    });
    $componentPath.=".class.php";
    // 按照物理路径导入
    return $cacheClass[$className]=require($componentPath);
}

/**
 * 根据用户目的执行相应功能
 * @param Intent $intent
 * @return bool
 */
function startIntent($intent){
    do{
        if(!$intent instanceof Intent){
            echo "500 服务器(框架)内部错误";
            return false;
        }
        if($intent->getState()!=200){
            header('HTTP/1.1 404 Not Found');
            header("status: 404 Not Found");
            echo $intent->getState().$intent->getMessage();
            return false;
        }
        $object=$intent->getObject();
        $action=$intent->getAction();
        if(empty($object)|| empty($action)){
            echo "路由解析错误";
            return false;
        }
        $intent=$object->exec($intent);
    }while($intent);
    return true;
}

/** @return AppInfo */
function &getAppInfo(){
    return $GLOBALS['globalAppInfo'];
}

// 引入一个模块，模块和页面采用同样的写法
function import_part($classFullName, $action, $param=null){
    import($classFullName);
    if($s=strrchr($classFullName, ".")){
        $className=substr($s, 1);
    }else{
        $className=$classFullName;
    }
    $object=call_user_func(array($className,"init"));
    $intent=new Intent(200,null,$object,$action,$param);
    startIntent($intent);
}

function updateClass(){
    $baseClass=getConfig('systemClass');
    // 获得整个结构目录
    $classes=ClassManager::analysisClass(array("custom",),function($name,DirectoryIterator $object){
        return strtr($object->getPath(),"/\\","..").'.'.$name;
    });

    // 将属性数组转化为扁平数组
    $userClass=array();
    foreach(new RecursiveIteratorIterator(new RecursiveArrayIterator($classes)) as $class=>$componentPath){
        $userClass[$class]=$componentPath;
    }
    return array_merge($baseClass,$userClass);
}

/**
 * Class Intent 记录着跳转请求
 */
class Intent {
    /** @var  RunnableInterface */
    protected $object;
    /** @var  String */
    protected $action;
    /** @var  array */
    protected $param=array();
    /** @var  int */
    protected $state;
    /** @var  String */
    protected $message;

    function __construct($state=200, $message='', $object=null, $action=null, $param=array()) {
        $this->state = $state;
        $this->message = $message;
        $this->object = $object;
        $this->action = $action;
        $this->param = $param?:array();
    }

    public function getAction() {
        return $this->action;
    }
    public function setAction($action) {
        $this->action = $action;
    }
    public function getObject() {
        return $this->object;
    }
    public function setObject($object) {
        $this->object = $object;
    }
    public function getParam() {
        return $this->param;
    }
    public function setParam($param) {
        $this->param = $param;
    }
    public function getMessage() {
        return $this->message;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
    public function getState() {
        return $this->state;
    }
    public function setState($state) {
        $this->state = $state;
    }
}
