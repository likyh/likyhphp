<?php
/**
 * 可运行的类必须实现的接口
 * @author linyh
 */
interface RunnableInterface {
    /**
     * 新建类
     * @return RunnableInterface
     */
    public static function init();

    /**
     * 运行该函数，该函数应该要可以接受任意多个变量
     * @param Intent $intent
     * @return Intent|null
     */
    public function exec($intent);

    // 释放当前类
    public static function release();
}

// 简单Cache接口
interface CacheInterface extends ArrayAccess {
    // 检查是否存在
    public function check($key);

    // 根据参数的类型获取一条记录
    public function get($key);

    // 添加\修改一条记录，如果不存在就添加，存在就替换
    public function set($key,$value);

    // 删除一个记录
    public function delete($key);
}
// 拓展Cache接口
interface CacheExtendInterface extends CacheInterface {
    /**
     * 通过前缀搜索值
     * @param $prefix_key Key值的前缀
     * @param int $count
     * @return mixed
     */
    public function getMulPrefix($prefix_key, $count);
    // 根据参数的类型获取一条或者多条记录，参数可以是一个数组或者一个字符串
    public function get($keys);

    /**
     * 添加\修改一条记录，如果不存在就添加，存在就替换
     * @param string $key 键名
     * @param string $value 值
     * @param int $timeout 过期的时间，可以为秒数或者unix时间戳
     */
    public function set($key,$value,$timeout=0);
    // 添加一个键值
    public function add($key,$value,$timeout=0);
    // 替换一个键值，替换成功返回true,如果原本不存在，则返回false
    public function replace($key,$value,$timeout=0);

    /**
     * 删除某一个特定的
     * @param $key
     * @param int $timeout 设置删除的时间
     */
    public function delete($key, $timeout=0);
    // 删除所有数据
    public function flush();
}
