<?php
/**
 * 定义系统函数
 * @author linyh
 */

/**
 * 获取该名称的配置信息
 * @param $configSet string 配置集合名称
 * @param $configName string|null 配置条目名称
 * @return string|array
 */
function getConfig($configSet, $configName=null, $process_sections=false){
    $config=&$GLOBALS['config'];
    if(!isset($config[$configSet])){
        $config[$configSet]=analysisIniFile("config/{$configSet}.config.ini",$process_sections);
    }
    // 判断是否获取整个数组
    if(empty($configName)){
        return $config[$configSet];
    }else{
        return isset($config[$configSet][$configName])?$config[$configSet][$configName]:null;
    }
}

/**
 * 分析Ini文件
 * @param string $path 文件地址
 * @param bool $process_sections
 * @return array 分析结果，二维数组
 */
function analysisIniFile($path,$process_sections=true){
    if(is_readable($path)){
        return parse_ini_file($path, $process_sections);
    }else{
        trigger_error("找不到指定的配置文件:{$path}",E_USER_WARNING);
        return array();
    }
}

/********** string password  *************/
function getHash($value){
    $value=func_get_args();  //返回全部参数的值,类型是数组
    return md5(md5(implode(",",$value)));
}
function getSaltHash($value){
    $salt=mt_rand(10000,100000);
    return md5($value.$salt);
}
function getSafeHash($value, $authKey='auth'){
    $auth=getConfig('password', $authKey);
    return sha1($auth.$value.$auth);
}
function getPassWord($user, $pass){
    $auth=getConfig('password', 'auth');
    return sha1($auth.$user.$auth.$pass.$auth);
}

/**
 * 只有数字字母下划线验证，也就是是否符合变量名
 * @param string $content 要过滤的字符串
 * @return string 返回是否符合，符合为true
 */
function validateVar($content) {
    return preg_match("/^[A-Za-z_][A-Za-z0-9_]*$/", $content);
}

// 比较$sourceString字符串是否以$compareString开头
function asciiStartWith($sourceString, $compareString){
    if(strlen($compareString)>strlen($sourceString)) return false;
    for($i=0; $i<strlen($compareString); ++$i){
        if($sourceString[$i]!=$compareString[$i]) return false;
    }
    return true;
}
// 比较$sourceString字符串是否以$compareString结尾
function asciiEndWith($sourceString, $compareString){
    if(strlen($compareString)>strlen($sourceString)) return false;
    for($i=strlen($sourceString)-1,$j=strlen($compareString)-1; $j>=0; --$i,--$j){
        if($sourceString[$i]!=$compareString[$j]) return false;
    }
    return true;
}

function json($s){
    return json_decode($s,true);
}

/********** array  *************/
/**
 * 字符串划分处理
 * @param string $delimiter 划分和连接使用的字符串
 * @param array|string $dealArray 需要处理的数组或者字符串
 * @param string|callable $dealFunction 处理函数，需要返回一个字符串
 * @return string 处理完的字符串
 */
function explodeDeal($delimiter, $dealArray, $dealFunction) {
    assert(is_callable($dealFunction)/*,'$dealFunction 参数需要可执行的'*/);
    if(is_string($dealArray)){
        $dealArray=explode($delimiter,$dealArray);
    }
    return implode($delimiter,array_map($dealFunction,$dealArray));
}

/**
 * 从数组中删除一个元素(键名为$key,键值为$value)，
 * 如果存在则执行函数，返回值为是否存在，
 * 如果两个均为空值，则必定不执行函数
 * 如果存在多个内容，只选择其中一个执行
 */
function arrayDeal(&$array, $callback, $key=null, $value=null){
    if(!is_null($key)){
        // 如果key存在，则看数组中是否存在键为key且值为value
        if(!array_key_exists($key,$array)|| !is_null($value)&&$array[$key]!=$value) return false;
        $value=$array[$key];
    }else{
        // 如果key不存在，则看数组中是否存在value
        if(is_null($value)|| !in_array($array,$value)) return false;
        $key=array_search($array,$value);
    }
    unset($array[$key]);
    call_user_func($callback, $key, $value);
    return true;
}
function arrayDealArray(&$array,$callback,$keyArray=array(),$valueArray=array()){
    foreach((array)$keyArray as $key) arrayDeal($array,$callback,$key,null);
    foreach((array)$valueArray as $value) arrayDeal($array,$callback,null,$value);
}
function arrayUnsetAndReturn(&$array,$key){
    $re=array_key_exists($key,$array)? $array[$key]: null;
    unset($array[$key]);
    return $re;
}