<?php
// 注意，下面这句话，除非你是框架和类库分开存放的，否则不需要使用
//ini_set("include_path", '.;F:\workspace\www\likyhPHP\\');

// 导入类库
require_once 'likyhphpLib.php';

// 配置信息
require_once 'config/run.config.php';

// 导入框架
require_once 'likyhphpFrame.php';
