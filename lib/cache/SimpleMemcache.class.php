<?php
if(!function_exists("memcache_init")){
    /**
     * 返回一个memcache的资源
     */
    function memcache_init(){
        // 在这个函数里面要做的是连接memcache服务器
        return null;
    }
}
class SimpleMemcache implements CacheInterface {
    /**
     * 用于记录保存的id的集合
     * @var string
     */
    protected $set;
    protected $site;
    protected $mmc;

    /**
     * @param string $set 要缓存的数据的分类
     */
    public function __construct($set){
        $this->set=$set;
        $this->site=getConfig("site","name");
        $this->mmc=memcache_init();
    }

    protected function keyHash($key){
        return getHash($this->site,$this->set,$key);
    }


    public function check($key) {
        // TODO: Implement check() method.
    }
    public function get($key){
        return memcache_get($this->mmc,$this->keyHash($key));
    }
    public function set($key,$value,$timeout=0){
        memcache_set($this->mmc, $this->keyHash($key), $value, MEMCACHE_COMPRESSED, $timeout);
    }
    public function delete($key, $timeout=0){
        memcache_delete($this->mmc,$key, $timeout);
    }

    // 删除所有数据
    public function flush(){
        memcache_flush($this->mmc);
    }

    public function offsetExists($offset){
        return $this->check($offset);
    }
    public function offsetGet($offset){
        return $this->get($offset);
    }
    public function offsetSet($offset, $value){
        $this->set($offset, $value);
    }
    public function offsetUnset($offset){
        $this->delete($offset);
    }
}