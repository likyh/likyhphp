<?php
// Redis Cache类，暂时只支持Bae的，别的平台没有测试过
class SimpleRedis implements CacheInterface {
    /** @var  Redis */
    protected $redis;
    /** @var  SimpleRedis */
    protected static $selfObject;

    public static function init(){
        if(!self::$selfObject){
            self::$selfObject=new SimpleRedis();
        }
        return self::$selfObject;
    }
    protected function __construct(){
        $config=getConfig("redis");
        $this->redis = new Redis();
        $this->redis->connect($config['host'], $config['port']);
        $this->redis->auth($config['user'] . "-" . $config['pwd ']. "-" . $config['dbname']);
    }

    public function check($key) {
        // TODO: Implement check() method.
    }
    public function get($keys){
        return is_array($keys)? $this->redis->getMultiple($keys): $this->redis->get($keys);
    }
    public function set($key,$value){
        return $this->redis->set($key,$value);
    }
    public function delete($key){
        $this->redis->delete($key);
    }

    public function offsetExists($offset){
        return $this->check($offset);
    }
    public function offsetGet($offset){
        return $this->redis->get($offset);
    }
    public function offsetSet($offset, $value){
        $this->redis->set($offset, $value);
    }
    public function offsetUnset($offset){
        $this->redis->delete($offset);
    }
}