<?php
/**
 * 文件cache
 */
class FileCache implements CacheInterface {
    protected $cacheFile;
    protected $data;
    protected $set;

    public function __construct($set){
        $cachePath=PathGeneration::getFolder("./cache/");
        $this->cacheFile=$cachePath."FileCache.tmp";
        $this->set=$set;
        if(is_readable($this->cacheFile)){
            $this->data=json_decode(file_get_contents($this->cacheFile),true);
        }else{
            $this->data=array();
        }
    }

    protected function getKey($key){
        return $this->set.$key;
    }

    public function check($key) {
        return isset($this->data[$this->getKey($key)]);
    }

    // 删除一个记录
    public function delete($key){
        if(!isset($this->data[$this->getKey($key)])){
            return false;
        }
        unset($this->data[$this->getKey($key)]);
        fwrite(fopen($this->cacheFile,"w"),json_encode($this->data));
        return true;
    }

    // 获取一个记录
    public function get($keys){
        $result=array();
        foreach((array)$keys as $key){
            if(isset($this->data[$this->getKey($key)])){
                array_push($result, $this->data[$this->getKey($key)]);
            }
        }
        return is_array($keys)?$result:reset($result);
    }

    // 添加\修改一条记录
    public function set($key,$value){
        $this->data[$this->getKey($key)]=$value;
        fwrite(fopen($this->cacheFile,"w"),json_encode($this->data));
        return true;
    }

    public function offsetExists($offset){
        return isset($this->data[$offset]);
    }
    public function offsetGet($offset){
        return $this->get($offset);
    }
    public function offsetSet($offset, $value){
        $this->data[$offset]=$value;
        fwrite(fopen($this->cacheFile,"w"),json_encode($this->data));
    }
    public function offsetUnset($offset){
        unset($this->data[$offset]);
        fwrite(fopen($this->cacheFile,"w"),json_encode($this->data));
        return true;
    }
}