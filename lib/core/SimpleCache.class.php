<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2015/5/28
 * Time: 12:42
 */
class SimpleCache {
    /** @var  CacheInterface */
    protected $driver;
    function __construct(CacheInterface $driver) {
        $this->driver = $driver;
    }

    /**
     * 单数据缓存，用于读取数据，不进行双向同步
     * @param string $key 要获取的键名
     * @param callable $dataFunction 接受的参数是$key,返回的数据必须要一个键名和一个值
     * @param int $timeout 过期时间，过期时间不建议少于60s,0就是不缓存
     * @param bool $readAlways 是否忽略读取锁立刻读取数据
     * @return mixed
     */
    function cache($key, Callable $dataFunction, $timeout=3600, $readAlways=false){
        $data=$this->driver->get($key);
        // 是不是需要重新读取的临近过期数据
        $ifDirtyData=!empty($data)&& ($data['readTime']<time()&&
                ($data['writeLock']==0|| $data['writeTime']>=time())
                || $readAlways);
        $ifRead=empty($data)|| $ifDirtyData|| $readAlways;
        if($ifDirtyData){
            // 设置数据查询锁
            $data['writeTime']=time()+20;
            $data['writeLock']=1;
            $this->driver->set($key,$data);
        }
        // 开始查询数据
        if($ifRead){
            $data['data']=call_user_func($dataFunction,$key);
            if(!empty($data['data'])){
                $data['readTime']=time()+$timeout;
                $data['writeTime']=0;
                $data['writeLock']=0;
                $this->driver->set($key,$data);
            }
        }
        return $data['data'];
    }

    /**
     * 一个列表的数据缓存，比如需要缓存一系列商品，用于读取数据，不进行双向同步
     * @param array $keys 要获取的键名数组
     * @param callable $dataFunction 接受的参数是$keys(一个数组),返回的数据必须要 键名和值的数组
     * @param int $timeout 过期时间,0就是不缓存
     * @return array
     */
    function listCache($keys, Callable $dataFunction, $timeout=3600){
        $dataArray=array();
        $pureDataArray=array();
        $keysNotFound=array();
        foreach($keys as $key){
            $dataArray[$key]=$this->driver->get($key);
            if(!empty($dataArray[$key])) $pureDataArray[$key]=$dataArray[$key]['data'];
            // 是不是需要重新读取的临近过期数据
            if(!empty($dataArray[$key])&& $dataArray[$key]['readTime']<time()&&
                ($dataArray[$key]['writeLock']==0|| $dataArray[$key]['writeTime']>=time())){
                // 设置数据查询锁
                $dataArray[$key]['writeTime']=time()+60;
                $dataArray[$key]['writeLock']=1;
                $this->driver->set($key,$dataArray[$key]);
            }
            $keysNotFound[]=$key;
        }
        // 开始查询、更新数据
        $dataTmp=call_user_func($dataFunction,$keysNotFound);
        foreach($dataTmp as $v){
        	list($key,$value)=$v;
            $pureDataArray[$key]=$value;
            $dataArray[$key]['data']=$value;

            $dataArray[$key]['readTime']=time()+$timeout;
            $dataArray[$key]['writeTime']=0;
            $dataArray[$key]['writeLock']=0;
            $this->driver->set($key,$dataArray[$key]);
        }
        return $pureDataArray;
    }
}
