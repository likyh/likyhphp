<?php
/** 
 * 标准库中组件的基础类，这是最简单就可以使用的Activity
 * @author linyh
 */
abstract class Activity implements RunnableInterface {
    /**
     * @var AppInfo
     */
    protected $appInfo;
    protected static $selfObjectArray;
    /**
     * 初始化函数，并返回当前类
     * @return mixed
     */
    public static function init(){
        $className=get_called_class();
        if(!isset(self::$selfObjectArray[$className])){
            self::$selfObjectArray[$className]=new $className();
        }
        return self::$selfObjectArray[$className];
    }

    public function exec($intent){
        $action=$intent->getAction();
        $action=$action?$action."Task":getConfig("site","defaultAction")."Task";
        if(!method_exists($this,$action)) {
            trigger_error("方法($action)不存在",E_USER_ERROR);
        }

        // 分析要执行的类的方法
        $className=get_called_class();
        $class=getAppInfo()->getSystemCache()->cache("{$className}ActivityTask",function(){
            return ClassManager::analysisActivity($this);
        },getConfig("site","systemCache"));

        $paramKey=$class[$intent->getAction()];
        $data=$intent->getParam();
        $paramValue=array();
        foreach($paramKey as $k) {
            $pn=$k['name'];
            $paramValue[] = isset($data[$pn])&&$data[$pn]!='' ? $data[$pn] : $k['default'];
        }
        return call_user_func_array(array(&$this, $action),$paramValue);
    }

    /**
     * 释放当前类
     */
    public static function release(){
        $className=get_called_class();
        unset(self::$selfObjectArray[$className]);
        return;
    }
}

/**
 * 组件，可以直接用于输出的类。
 */
abstract class Widget {
    abstract function __toString();
}
