<?php
// 应用程序基本信息，作为html输出的system变量
class AppInfo implements ArrayAccess {
    /**
     * 网站根目录
     * @var string
     */
    public $root;
    public $commonPath;
    public $scheme;
    public $host;
    public $siteRoot;

    /** @var SimpleCache */
    public $systemCache;
    /**
     * @param $set
     * @return CacheInterface
     */
    public $getCacheFunction;

    protected static $selfObject;
    public static function init(){
        return !self::$selfObject? self::$selfObject=new AppInfo(): self::$selfObject;
    }

    function __construct(){
        $this->commonPath=getConfig('site','commonPath');
        $this->scheme=isset($_SERVER["REQUEST_SCHEME"])?$_SERVER["REQUEST_SCHEME"]:'http';
        $this->host=$_SERVER["HTTP_HOST"];
        $this->root="{$this->scheme}://{$this->host}/";
        if(!empty($this->commonPath)) $this->root.=$this->commonPath."/";
        $this->siteRoot=$this->root."site/";

        $this->getCacheFunction=getConfig('cache','getCache');
        $this->systemCache=new SimpleCache($this->getCacheHandle("system"));
    }

    // 获取运行的根目录
    public function getRoot(){
        return $this->root;
    }
    public function getSiteRoot() {
        return $this->siteRoot;
    }

    // 检测是不是开发调试环境
    public function getIfDevelop(){
        return (bool)RUNNING_IN_DEVELOP;
    }

    /**
     * @param string $set 需要创建的缓存所属集合
     * @return CacheInterface
     */
    public function getCacheHandle($set){
        return call_user_func($this->getCacheFunction,$set);
    }
    public function getSystemCache(){
        return $this->systemCache;
    }

    // 获取ip 一个字符串
    public static function getIp(){
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $cip = $_SERVER["HTTP_CLIENT_IP"];
        }elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }elseif(!empty($_SERVER["REMOTE_ADDR"])) {
            $cip = $_SERVER["REMOTE_ADDR"];
        }else{
            return false;
        }
        return $cip;
    }

    public function offsetExists($offset) {
        return method_exists($this,"get".ucfirst($offset));
    }
    public function offsetGet($offset) {
        $getMethod="get".ucfirst($offset);
        return method_exists($this,$getMethod)? $this->$getMethod(): null;
    }
    public function offsetSet($offset, $value) {
        return;
    }
    public function offsetUnset($offset) {
        return;
    }
}
