<?php
/**
 * 系统web请求分析类
 * @author linyh
 */
class WebRouter extends Activity {
    protected $commonPath;
    protected static $selfObject;

    protected $ifAnalysis=false;
    protected $appInfo;
    protected $folderArray=array();
    protected $action;
    protected $root;

    /**
     * 根据配置文件清理url
     * @param string $requestStr 要清理的url
     * @return string
     */
    protected function urlClean($requestStr){
        $startAt=$requestStr[0]=="/"?1:0;
        $endAt=$requestStr[strlen($requestStr)-1]=="/"?-1:strlen($requestStr);
        // 检测一下开头是否有公共字符串
        if(!asciiStartWith(substr($requestStr, $startAt), $this->commonPath))
            trigger_error("配置文件中commonPath配置错误", E_USER_ERROR);
        $requestStr=substr($requestStr, $startAt+strlen($this->commonPath), $endAt);
        if(isset($requestStr[0])&&$requestStr[0]=="/"){
            return substr($requestStr, 1);
        }else{
            return $requestStr;
        }
    }

    /**
     * 构造函数，检查配置是否完成
     */
    protected function __construct(){
        $this->appInfo=&getAppInfo();
        $this->commonPath=getConfig('site',"commonPath");
        $this->root=$this->appInfo['root'];
        $this->ifAnalysis=false;
    }

    /**
     * @return WebRouter
     */
    public static function init(){
        return parent::init();
    }

    protected function transUrl($folderArray){
        // 分析custom类的结构
        $class=getAppInfo()->getSystemCache()->cache("ActivityIndex-WebRouter",function(){
            $classes=ClassManager::analysisClass(array("custom",),function($name,DirectoryIterator $object){
                return strtr($object->getPath(),"/\\","..").'.'.$name;
            });
            return $classes['custom'];
        },getConfig("site","systemCache"));
        $folderArray=array_filter($folderArray);
        $pathArray=array();
        $ifUseDefaultPage=false;
        // 比较url和缓存的路径数组
        do{
            $current=array_shift($folderArray);
            if(empty($current)&&!$ifUseDefaultPage){
                $current=getConfig('site', 'defaultPage');
                $ifUseDefaultPage=true;
            }
            if(!isset($class[$current])) return null;
            $class=&$class[$current];
            $pathArray[]=$current;
        }while(!is_string($class));
        // 返回值分别是解析路径数组，spacePath，方法
        return array($pathArray,$class,array_shift($folderArray));
    }

    /**
     * 返回router解析出来的
     * @return Intent
     */
    public function getTransformClass() {
        //分析目录
        $folderArray=$this->folderArray;
        list($path,$spacePath,$this->action)=$this->transUrl($folderArray);
        if(empty($path)) return null;
        $this->folderArray=$path;

        $page=array_pop($path);

        if(!import($spacePath)) {
            trigger_error("未找到该对象", E_USER_NOTICE);
            return null;
        }
        $object=call_user_func(array($page,"init"));
        // 检查方法是否存在以及是否应该采用默认方法
        $default=getConfig('site', 'defaultAction');
        if(!method_exists($object,$this->action."Task")) {
            if(method_exists($object, $default."Task")) {
                $this->action = $default;
            } else {
                return null;
            }
        }
        return new Intent(200,null,$object,$this->action,$_REQUEST);
    }

    /**
     * 执行这个路由并分析出指向的类
     */
    function webTask(){
        if(isset($_SERVER["HTTP_X_ORIGINAL_URL"])&& !empty($_SERVER["HTTP_X_ORIGINAL_URL"])){
            $URI=$_SERVER["REQUEST_URI"];
        }elseif(isset($_SERVER["REQUEST_URI"])&& !empty($_SERVER["REQUEST_URI"])){
            $URI = $_SERVER["REQUEST_URI"];
        }else{
            trigger_error("http请求中没有REQUEST_URI",E_USER_WARNING);
            $URI='';
        }
        $requestStr=$this->urlClean($URI);

        // 请求地址字符串
        $folderStr=strpos($requestStr,"?")!==false?substr($requestStr, 0, strpos($requestStr,"?")):$requestStr;

        $this->folderArray=!empty($folderStr)?explode("/",$folderStr):array();
        $intent=$this->getTransformClass();
        return $intent?:new Intent(404,"请求未找到");
    }

    private function paramImplode($param){
        return implode("&",array_map(array($this,"paramArrayImplode"),array_keys($param), array_values($param)));
    }
    private function paramArrayImplode($key,$value){
        if(is_int($key)){
            return $value;
        }elseif(is_bool($value)){
            return $key."=".($value?1:0);
        }elseif(!is_array($value)){
            return $key."=".urlencode($value);
        }
        $tmpArray=array();
        foreach($value as $k=>$v){
            $tmpArray[]=$this->paramArrayImplode($key."[".urlencode($k)."]",$v);
        }
        return implode("&",$tmpArray);
    }

    /**
     * 生成这个类可以解析的url
     * @param null|string $set set字符串
     * @param null|string $page page字符串
     * @param null|string $action action字符串
     * @param null|string|array $qus get参数，可以是一个字符串或者一个数组
     * @return string
     */
    public function getURL($set=null, $page=null, $action=null, $qus=null){
        $path=$this->root;
        $folderArray=$this->folderArray;
        if(is_null($page)){
            $page=array_pop($folderArray);
        }else{
            array_pop($folderArray);
        }
        if(!is_null($set)) $folderArray=explode("/",$set);
        array_push($folderArray, $page, !empty($action)?$action:$this->action);
        $path.= implode("/",$folderArray);

        $qusStr='?';
        if(!empty($qus)){
            $qusStr .= is_array($qus)?$this->paramImplode($qus):$qus;
            return $path.$qusStr;
        }else{
            return $path;
        }
    }
    public function getPage($page=null, $action=null, $qus=null){
        return $this->getURL(null,$page,$action,$qus);
    }
    public function getAction($action=null, $qus=null){
        return $this->getURL(null,null,$action,$qus);
    }
    public function getQuestion($qus=null){
        return $this->getURL(null,null,null,$qus);
    }
    public function getAbsolute($basePath){
        return $this->root.$basePath;
    }
}