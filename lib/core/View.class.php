<?php
/** 
 * 标准库中组件的基础类
 * @author linyh
 */
class View {
    protected static $selfObjectArray;
    public static function init(){
        $className=get_called_class();
        if(!isset(self::$selfObjectArray[$className])){
            self::$selfObjectArray[$className]=new $className();
        }
        return self::$selfObjectArray[$className];
    }

    protected static $lastResult;
    public static $debug=false;
    public static function getLastResult(){
        return self::$lastResult;
    }

    public static function displayAsHtml($result, $tplFile=null){
        if(self::$debug) return self::$lastResult=$result;
        $ha=HTMLAnalysis::init();
        $ha->display($result, "./site/".$tplFile);
        return true;
    }
    public static function formatAsHtml($result, $tplFile=null){
        if(self::$debug) return self::$lastResult=$result;
        $ha=HTMLAnalysis::init();
        ob_start();
        $ha->display($result, "./site/".$tplFile);
        $s=ob_get_clean();
        return $s;
    }
    public static function displayAsTips($url, $message="", $refresh=false, $state=DataMessage::STATE_SUCCESS, $html5=true){
        if(empty($message)){
            header("Location:$url");
        }elseif($html5){
            self::displayAsHtml(array("url"=>$url,"message"=>$message,"refresh"=>$refresh), "plugin/state/tips_html5.php");
        }else{
            self::displayAsHtml(array("url"=>$url,"message"=>$message,"refresh"=>$refresh), "plugin/state/tips.php");
        }
    }
    public static function displayAsJson($result){
        if(self::$debug) return self::$lastResult=$result;

        $time=time();
        $result['serverTime']=$time;
        $result['serverDate']=date("Y-m-d H:i:s", $time);

        if(isset($_GET["callback"])&& preg_match("/^[\\w_.]+$/", $_GET["callback"])) {
            echo htmlspecialchars($_GET["callback"]) . '(' . json_encode($result) . ')';
        }else{
            echo json_encode($result);
        }
        return true;
    }
    public static function displayDataMessage(DataMessage $dataMessage, $refresh=false){
        self::displayAsTips("",$dataMessage->title,$refresh,$dataMessage->state);
    }
    public static function displayDataMessageJson(DataMessage $dataMessage){
        self::displayAsJson(array(
            "state"=>$dataMessage->state,
            "title"=>$dataMessage->title,
        ));
    }
}