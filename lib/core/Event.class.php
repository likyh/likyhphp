<?php
/** 
 * 事件队列，既支持主动获取事件，也支持被动接收事件
 * TODO 未完成多进程获取事件互斥
 * TODO 暂时没做内存缓存 (SimpleCache)
 * @author linyh
 */
class EventManager {
    protected static $selfObject;
    /** @return EventManager */
    public static function init(){
        return !empty(self::$selfObject)? self::$selfObject: self::$selfObject= new EventManager();
    }

    // 添加向其他请求的事件
    public function pushEvent(EventHandle $event){
        $event->setState("ready");
        $event->commit();
    }
    // 添加需要其他服务主动请求的事件
    public function pendEvent(EventHandle $event){
        $event->setState("pend");
        $event->commit();
    }
    // 标记事件完成，自动根据状态设置不同的状态
    public function doneEvent(EventHandle $event){
        if($event->state=="ready"){
            $event->setState("push");
        }elseif($event->state=="pend"){
            $event->setState("done");
        }
        $event->setDoneTime();
        $event->commit();
    }

    // 各种获取事件的方法
    public function getEventById($id){
        return EventHandle::getEventById($id);
    }
    public function getEventByState($state="pend"){
        $eventList=EventHandle::getEvent(array("state"=>$state),1);
        return reset($eventList);
    }
    public function getEventByType($type,$sub_type,$state="pend"){
        $eventList=EventHandle::getEvent(array("type"=>$type,"sub_type"=>$sub_type,"state"=>$state),1);
        return reset($eventList);
    }
    public function getNewEvent($type,$sub_type,$data,$moreField=array()){
        return EventHandle::getNewEvent($type,$sub_type,$data,$moreField);
    }
    public function getEventList($condition,$limit=null){
        return EventHandle::getEvent($condition,$limit);
    }
}
/** Event数据库管理类 */
class EventModule extends DbData {
    protected static $selfObjectArray;
    /** @return EventModule */
    public static function init($tableName = 'likyh_event'){
        if(!isset(self::$selfObjectArray[$tableName])){
            self::$selfObjectArray[$tableName]=new EventModule($tableName);
        }
        return self::$selfObjectArray[$tableName];
    }

    //若无表建立通用表
    public function install(){
        $sql="CREATE TABLE IF NOT EXISTS `likyh_event` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `type` varchar(255) NOT NULL,
            `sub_type` varchar(255) DEFAULT NULL,
            `data` text,
            `state` enum('pend','done','ready','push','other') NOT NULL,
            `create_time` timestamp NULL DEFAULT NULL,
            `select_time` timestamp NULL DEFAULT NULL,
            `done_time` timestamp NULL DEFAULT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记录事件队列的表';";
        $this->db->sqlExec($sql);
        return true;
    }

    // 确定表名，确定字段数组
    protected $tableName;
    function __construct($tableName){
        parent::__construct();
        $this->tableName=$tableName;
        $this->tableInfo=array(
            "$tableName LE"=>array("id","type","sub_type","data","state","create_time","select_time","done_time","*")
        );
        $this->referInfo=array();
    }
    // 获取列表
    public function getList($tableArray=array("LE"), $condition, $sortArray=array(), $rows=null, $offset=0){
        $selectSql=$this->getFormatSelectSql(array("LE"));
        $this->paramFilter($condition,array(),reset($this->tableInfo));
        $selectSql->addConditionArray($condition);
        $selectSql->setSortBy($sortArray);
        $sql=$selectSql->getSql($rows,$offset);
        return $this->db->getList($sql);
    }
    // 获取详细信息
    public function getDetail($ids){
        $selectSql=$this->getFormatSelectSql(array("LE"));
        $selectSql->addConditionId($ids);
        $sql=$selectSql->getSql();
        $re=$this->db->getAll($sql);
        return is_array($ids)?$re:reset($re);
    }
    // 新建或者更新
    public function update($data){
        $re=parent::update($data);
        if($re>0){
            return new DataMessage(DataMessage::STATE_SUCCESS,"添加/修改成功",array("modifyLine"=>$re));
        }else{
            return new DataMessage(DataMessage::STATE_ERROR,"添加/修改失败",array("modifyLine"=>$re));
        }
    }
    // 删除
    public function delete($id){
        $data=$this->getDetail($id);
        $re=parent::delete($data);
        if($re>0){
            return new DataMessage(DataMessage::STATE_SUCCESS,"删除成功",array("modifyLine"=>$re));
        }else{
            return new DataMessage(DataMessage::STATE_ERROR,"删除失败",array("modifyLine"=>$re));
        }

    }
}
/** Event类 */
class EventHandle {
    public $id;
    public $type;
    public $sub_type;
    public $data;
    public $state;
    public $create_time;
    public $select_time;
    public $done_time;
    public $moreField;

    public static $stateEnum;

    /** @var EventModule */
    public static $eventMode;
    protected function __construct($data){
        arrayDealArray($data,function($key,$value){
            $this->$key=$value;
        },array("id","type","sub_type","data","state","create_time","select_time","done_time",));
        $this->setState();
        $this->moreField=$data;
    }

    // 根据id获取类的对象
    public static function getEventById($ids){
        $list=self::$eventMode->getDetail((array)$ids);
        $eventList=array();
        foreach($list as $eventData){
            $event=new EventHandle($eventData);
            $event->setSelectTime();
            $eventList[]=$event;
        }
        return is_array($ids)? $eventList: reset($eventList);
    }
    public static function getEvent($condition,$limit=null){
        list($list,$total)=self::$eventMode->getList(null,$condition,array(),$limit,0);
        $eventList=array();
        foreach($list as $eventData){
            $event=new EventHandle($eventData);
            $event->setSelectTime();
            $eventList[]=$event;
        }
        return $eventList;
    }

    // 根据id获取类的对象，注意最后一个也是需要数据库按照键值对的形式支持的
    public static function getNewEvent($type,$sub_type,$data,$moreField=array()){
        $event=new EventHandle(array(
            "id"=>null,
            "type"=>$type,
            "sub_type"=>$sub_type,
            "data"=>$data,
            "state"=>"other",
        ));
        $event->setCreateTime();
        $event->moreField=$moreField;
        return $event;
    }

    // 保存修改
    public function commit(){
        $data=array(
                "id"=>$this->id,
                "type"=>$this->type,
                "sub_type"=>$this->sub_type,
                "data"=>$this->data,
                "state"=>$this->state,
                "create_time"=>$this->create_time,
                "select_time"=>$this->select_time,
                "done_time"=>$this->done_time,
            )+$this->moreField;
        return self::$eventMode->update($data);
    }

    // 设置事件状态
    public function setState($state=null) {
        if(empty($state)) $state=$this->state;
        $this->state = in_array($state,self::$stateEnum)? $state: "other";
    }
    // 设置创建时间
    public function setCreateTime() {
        $this->create_time=date("Y-m-d H:i:s");
    }
    // 设置查询时间
    public function setSelectTime() {
        $this->select_time=date("Y-m-d H:i:s");
    }
    public function setDoneTime() {
        $this->done_time=date("Y-m-d H:i:s");
    }
}
EventHandle::$eventMode=EventModule::init();
// 几个状态分别表示 (被动)等待请求 已经完成 (主动)等待推送 已经推送 其他状态
EventHandle::$stateEnum=array("pend","done","ready","push","other",);