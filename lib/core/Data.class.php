<?php
/**
 * 所有的 Mode/Module 即数据处理类都将继承自这里
 * Class Data
 */
abstract class Data {
    // 继承自Data且未覆盖init方法的实例保存在这里面
    protected static $selfObjectArray;
    public static $staticDb;
    /** @var SqlDB */
    public $db;

    // 初始化对象
    protected function __construct() {
        $this->onStart();
        $this->db=$this->getStaticDb();
    }

    protected function getStaticDb(){
        if(empty(self::$staticDb)){
            self::$staticDb=SqlDB::init();
        }
        return self::$staticDb;
    }

    /**
     * 初始化函数，并返回当前类
     * @return Data
     */
    public static function init(){
        $className=get_called_class();
        if(!isset(self::$selfObjectArray[$className])){
            self::$selfObjectArray[$className]=new $className();
        }
        return self::$selfObjectArray[$className];
    }

    /**
     * 在准备运行前调用
     * 注意：覆盖此函数时，建议调用父类方法（parent::onStart();）
     */
    protected function onStart(){
    }
}
// 用于构建数据库Mode
abstract class DbData extends Data{
    /**
     * 里面记录了当前表，可以在一个字段的最后加一个*, 将会查询所有，并把所有的修改字段都放在他里面
     * ！ 第一个添加的表被认为是主表，在插入/修改的时候，会对其他表传递该表的id
     * 注意，只允许有一个表加*
     */
    protected $tableInfo;
    protected $referInfo;
    protected function __construct(){
        parent::__construct();
    }
    // 尽量自动填充尽可能多的东西
    public function getFormatSelectSql($tableArray){
        $selectSql=new SqlDBSelectSql();
        $data=$this->tableFilter($tableArray);
        $selectSql->addTableArray($data);
        $selectSql->addReferArray($this->referInfo);
        return $selectSql;
    }

    // 获取列表
    public abstract function getList($tableArray, $condition, $sortArray=array(), $rows=null, $offset=0);
    public abstract function getDetail($ids);

    // 新建或者更新 TODO 最好还是返回DataMessage
    public function update($data){
        $tableData=$this->formatByTableInfo($data);
        $publicId=null;
        $re=0;
        foreach($tableData as $table=>$tData){
            // 填充主表id 填充$id$内容，实现的非常简单
            $publicKey= array_search('#id#',$tData);
            if($publicKey!==false) $tData[$publicKey]=$publicId;

            $id=arrayUnsetAndReturn($tData,"id");
            $re+=$this->db->modify($table,$id,$tData);

            // 保存主表id
            if(!$publicId) $publicId= empty($id)? $this->db->insertId(): $id;
        }
        return $re;
    }

    // 对表删除内容（如果只有一个表不建议使用）
    public function delete($data){
        $tableData=$this->formatByTableInfo($data);
        $re=0;
        foreach($tableData as $table=>$tData){
            $id=arrayUnsetAndReturn($tData,"id");
            $re+=$this->db->delete($table,$id);
        }
        return $re;
    }

    // 对表格进行筛选
    protected function tableFilter($tableArray){
        $tmpDataArray=array();
        foreach($this->tableInfo as $k=>$field){
            list($tableName,$tableNameAlias)=SqlDBSelectSql::divideTableName($k);
            if(in_array($tableName,$tableArray)|| in_array($tableNameAlias,$tableArray)){
                $tmpDataArray[$k]=$field;
            }
        }
        return $tmpDataArray;
    }

    // 对根据tableInfo把$paramArray(要插入的数据)划分成一个个表
    protected function formatByTableInfo(&$paramArray){
        $formatParamArray=array();
        // 默认的表名，即字段包含*的表名
        $defaultTableName="";
        foreach($this->tableInfo as $k=>$field){
            // 依次查找每一个表
            list($tableName,$tableNameAlias)=SqlDBSelectSql::divideTableName($k);
            if(array_search("*",$field)!==false) $defaultTableName=$tableName;
            foreach($paramArray as $param=>$value){
                if(!validateVar($param)) continue;
                //如果表中存在键，则保存 数据库字段名=>值
                $paramKey=array_search($param,$field);
                if($paramKey!==false){
                    unset($paramArray[$param]);
                    $fieldName=is_numeric($paramKey)? $param: $paramKey;
                    $formatParamArray[$tableName][$fieldName]=$value;
                }
            }
        }
        if($defaultTableName!=null&& isset($formatParamArray[$defaultTableName])){
            $formatParamArray[$defaultTableName]+=$paramArray;
        }

        // 根据关联信息对各个表加上关联键
        $firstBool=true;
        foreach($this->tableInfo as $k=>$fields){
            if($firstBool){
                $firstBool=false;
                continue;
            }
            list($tableName,$tableNameAlias)=SqlDBSelectSql::divideTableName($k);
            if(!isset($formatParamArray[$tableName])) continue;
            foreach($fields as $field){
                if(isset($this->referInfo[$field])|| array_search($field, $this->referInfo)){
                    $formatParamArray[$tableName][$field]='#id#';
                    break;
                }
            }
        }
        return $formatParamArray;
    }

    /**
     * 对字段按照自定义的内容筛选，数组要求如下
     * @param array $param 要筛选的字段键值对数组
     * @param array $require 必须要的内容
     * @param array $optional 可选的内容
     * @return array
     */
    public function paramFilter($param, $require, $optional){
        $filter=array();
        foreach($require as $paramKey){
            $filter[$paramKey]=isset($param[$paramKey])? $param[$paramKey]: null;
        }
        foreach($param as $paramKey){
            if(in_array($paramKey,$optional)){
                $filter[$paramKey]=$param[$paramKey];
            }
        }
        return $filter;
    }
}
// 用于返回一个操作成功失败、提示语、数据
class DataMessage {
    const STATE_SUCCESS=200;
    const STATE_INFO=300;
    const STATE_WARRING=400;
    const STATE_ERROR=500;

    public $state;
    public $title;
    public $data;

    /**
     * @param int $state 完成状态
     * @param string $title 提示语
     * @param array $data 获取的数据，用于操作并返回数据的情况
     */
    function __construct($state, $title, $data=null) {
        $this->state = $state?:self::STATE_SUCCESS;
        $this->title = !empty($title)? $title: null;
        $this->data = !empty($data)? (array)$data: null;
    }

    // 增加一个$title到原本的内容里，参数可以是一个数组或者一个字符串
    public function addTitle($title) {
        $this->title=empty($this->title)? $title: array_merge((array)$this->title, (array)$title);
    }
    public function getTitle() {
        return $this->title;
    }
    public function setTitle($title){
        $this->title = $title;
    }

    // 获取设置查询state
    public function getState() {
        return $this->state;
    }
    public function setState($state){
        $this->state = $state;
    }
    public function judgeState($state=self::STATE_SUCCESS){
        return $this->state==$state;
    }

    public function getData() {
        return $this->data;
    }
    public function setData($data) {
        $this->data = $data;
    }
    public function addData($key, $value) {
        $this->data[$key] = $value;
    }
}
