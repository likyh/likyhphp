<?php
/**
 * 搜索所有类文件并按照目录索引
 */
class ClassManager {

	//获得指定目录里面所有类并生成指定格式
	static function analysisClass($analysisArray, $dealFunc){
        $classArray=array();
        foreach($analysisArray as $v){
            self::analysisPath($classArray[$v], $v, $dealFunc);
        }
        return $classArray;
	}

    protected static function analysisPath(&$classArray, $dir, $dealFunc){
        $classArray=array();
        $objects=new DirectoryIterator($dir);
        foreach($objects as $object){
            if($object->isFile()){
                $name=$object->getBasename(".class.php");
                if (validateVar($name)) {
                    $classArray[$name]=call_user_func($dealFunc, $name, $object);
                }
            }elseif($object->isDir()&&!$object->isDot()){
                self::analysisPath($classArray[$object->getFilename()],$object->getPath()."/".$object->getFilename(),$dealFunc);
            }
        }
    }

    public static function analysisActivity($object){
        $class=new ReflectionClass($object);
        if(!$class->isSubclassOf("Activity")) return array();
        // 只有Activity类的子类，对方法以及参数进行分析并记录
        $func=array();
        foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
            if(asciiEndWith($method->getName(), "Task")) {
                $methodName = substr($method->getName(), 0, -4);
                $func[$methodName] = array();
                $params=$method->getParameters();
                foreach ($params as $param) {
                    $func[$methodName][]=array(
                        'name'=> $param->getName(),
                        'default'=> $param->isDefaultValueAvailable()?$param->getDefaultValue():null,
                    );
                }
            }
        }
        return $func;
    }
}
