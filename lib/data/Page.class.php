<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2014/12/16
 * Time: 16:07
 */
class Page {
    /** @var  PageWidget */
    protected $pageWidget;

    protected $currentPage;
    protected $pageSize;
    // 页码生成函数，临时保存一下
    public $pageCallback;

    /**
     * 构造函数，传入参数以便使用
     * @param int $currentPage 当前页码
     * @param string $pageUrl 传递一个带有#page#的url,页码会自动填入#page#地方
     * @param int $pageSize 每页的大小
     * @param int $total 总数
     */
    function __construct($currentPage=1, $pageUrl=null, $pageSize=30, $total=null) {
        $this->setPageUrl($pageUrl);
        $this->setOffset($currentPage,$pageSize);
        if(isset($total)){
            $this->setTotal($total);
        }
    }

    public function setPageCallback($pageCallback) {
        $this->pageCallback = $pageCallback;
        if(isset($this->pageWidget)){
            $this->pageWidget->pageCallback=$pageCallback;
        }
    }
    // 传递一个带有#page#的url,页码会自动填入#page#地方
    public function setPageUrl($url){
        $this->setPageCallback(function($page)use($url){
            return str_replace("#page#",$page,$url);
        });
    }

    // 设置当前页数以及每一页的大小
    public function setOffset($page,$pageSize){
        $this->currentPage = $page>0? $page:1;
        $this->pageSize=$pageSize>0? $pageSize:1;
    }
    public function getOffset(){
        return ($this->currentPage-1)*$this->pageSize;
    }
    public function getPageSize(){
        return $this->pageSize;
    }

    public function setTotal($total){
        // 新建并传递数据
        $pageWidget=new PageWidget($this->currentPage, $this->pageSize, $this->pageCallback);

        $pageWidget->total=$total=$total>0?$total:0;
        // 计算总页数
        $pageWidget->totalPage=ceil($total/$this->pageSize);

        // 校正当前页数
        $pageWidget->currentPage = $pageWidget->currentPage <= $pageWidget->totalPage?
            $pageWidget->currentPage: $pageWidget->totalPage;
        $this->pageWidget=$pageWidget;
    }

    // 返回用于显示和输出的Widget
    // 如果获取到null不要紧张，只有在传递总数以后才会生成
    public function getWidget($pageOffset=4, $className=null){
        $this->pageWidget->pageOffset=$pageOffset;
        $this->pageWidget->className=$className;
        return $this->pageWidget;
    }
}
class PageWidget extends Widget implements Iterator {
    public $currentPage;
    public $total;
    public $pageSize;
    // 总页数
    public $totalPage;
    // 显示页码周围的页数
    public $pageOffset=4;
    // 页码生成函数
    public $pageCallback;
    // 自动生成的html的类名
    public $className;
    // 控制显示的内容
    protected $ifText=true;
    protected $ifFirst=true;
    protected $ifShowPrev=true;

    function __construct($currentPage, $pageSize, $pageCallback) {
        $this->currentPage = $currentPage;
        $this->pageSize = $pageSize;
        $this->pageCallback = $pageCallback;
    }

    public function setDisplayContent($ifText=true, $ifFirst=true, $ifPrev=true){
        $this->ifText=$ifText;
        $this->ifFirst=$ifFirst;
        $this->ifShowPrev=$ifPrev;
        return $this;
    }

    public function getCurrentPage(){ return $this->currentPage; }
    public function getTotal(){ return $this->totalPage; }
    public function ifPrev(){ return $this->currentPage>1; }
    public function getPrev(){ return $this->currentPage-1; }
    public function ifNext(){ return $this->currentPage< $this->totalPage; }
    public function getNext(){ return $this->currentPage+1; }
    public function getFirst(){ return 1; }
    public function getLast(){ return $this->totalPage; }

    // 置显示页码周围的页数
    public function setPageOffset($pageOffset) {
        $this->pageOffset = $pageOffset;
    }

    function getPageUrl($page){
        if(!empty($this->pageCallback)&& is_callable($this->pageCallback)){
            return call_user_func($this->pageCallback, $page);
        }
        return null;
    }
    function __toString() {
        $s="<div class='dataPage {$this->className}'><ul>";
        if($this->ifText) $s.="<li>共<span>{$this->getLast()}</span>页/<span>{$this->total}</span>条记录
            当前是第<span>{$this->getCurrentPage()}</span>页
        </li>";
        if($this->total>0) {
            if($this->ifFirst) $s.="<li><a href='{$this->getPageUrl($this->getFirst())}'>首页</a></li>";
            if($this->ifShowPrev&& $this->ifPrev()) $s.="<li class='page-up'><a href='{$this->getPageUrl($this->getPrev())}'>上一页</a></li> ";
            foreach($this as $i){
                $s.="<li class='page'><a href='{$this->getPageUrl($i)}'>{$i}</a></li> ";
            }
            if($this->ifShowPrev&& $this->ifNext()) $s.="<li class='page-down'><a href='{$this->getPageUrl($this->getNext())}'>下一页</a></li> ";
            if($this->ifFirst) $s.="<li><a href='{$this->getPageUrl($this->getLast())}'>末页</a></li>";
        }
        $s.="</ul></div>";
        return $s;
    }

    protected $now;
    protected $start;
    protected $end;
    public function current() {
        return $this->now;
    }
    public function next() {
        $this->now++;
    }
    public function key() {
        return null;
    }
    public function valid() {
        return $this->now>=$this->start && $this->now<=$this->end;
    }
    public function rewind() {
        // 初始化内容，从第一个开始循环
        $start= $this->currentPage- $this->pageOffset;
        $this->start= $start>0? $start:1;

        $end= $start+ $this->pageOffset+ $this->pageOffset;
        $this->end= $end<=$this->totalPage? $end:$this->totalPage;

        $this->now=$this->start;
    }
}
