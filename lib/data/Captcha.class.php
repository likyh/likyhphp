<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2015/6/3
 * Time: 18:15
 */

class Captcha {
    const TYPE_NUMBER_6=0;
    const TYPE_NUMBER_8=1;
    const TYPE_CHAR_6=2;

    public static function getCaptcha(){
        // TODO 直接输出一个带有验证码的图片，可以加上各种混乱符号
    }

    // 生成验证码，remark是一个标记，随便给一个内容就可以，在检查的时候，remark不同就认为不匹配
    public static function getCode($remark, $type=self::TYPE_NUMBER_6){
        switch($type){
            case self::TYPE_NUMBER_6: $code=rand(100000, 1000000); break;
            case self::TYPE_NUMBER_8: $code=rand(10000000, 100000000); break;
            default: case self::TYPE_CHAR_6: $code=substr(md5(rand(100000, 1000000).time()),0,6); break;
        }
        $_SESSION[CONFIG_SITE_NAME][$remark]['code']=$code;
        $_SESSION[CONFIG_SITE_NAME][$remark]['time']=20;
        return $code;
    }
    // 检查验证码
    public static function checkCode($remark, $code){
        if(!isset($_SESSION[CONFIG_SITE_NAME][$remark]['code'])|| empty($_SESSION[CONFIG_SITE_NAME][$remark]['code'])){
            return new DataMessage(DataMessage::STATE_WARRING, "验证码尚未发送");
        }
        if($_SESSION[CONFIG_SITE_NAME][$remark]['time']--<=0){
            return new DataMessage(DataMessage::STATE_WARRING, "验证码验证次数过多，请重新发送");
        }
        return $_SESSION[CONFIG_SITE_NAME][$remark]['code']==$code?
            new DataMessage(DataMessage::STATE_SUCCESS, '验证码正确'):
            new DataMessage(DataMessage::STATE_ERROR, '验证码错误');
    }
}