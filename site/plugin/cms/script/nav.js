/**
 * Created by linyh on 2015/7/25.
 */
//导航栏
define(["jquery"],function($){
    var nav={
        boxName:'menuItem',
        subName:'subItem',
        subBoxName:'subItemBox',
        clickAreaName:'title',
        stateName:'state',
        getBoxes:function(num){
            if(typeof num == 'undefined'){
                return document.getElementsByClassName(nav.boxName);
            }else{
                return document.getElementsByClassName(nav.boxName)[parseInt(num,10)];
            }
        },
        getSubItem:function(box){
            if(typeof box == 'undefined'){
                box = document;
            }
            return box.getElementsByClassName(nav.subName);
        },
        getSubBox:function(box){
            if(typeof box == 'undefined'){
                box = document;
            }
            return box.getElementsByClassName(nav.subBoxName)[0];
        },
        getClickArea:function(box){
            if(typeof box == 'undefined'){
                box = document;
            }
            return box.getElementsByClassName(nav.clickAreaName)[0];
        },
        stateChange:function(state,num){
            console.log(num);

            console.log(state);
            var box = nav.getBoxes(num);
            var clickArea = nav.getClickArea(nav.getBoxes(num));
            var subBox = nav.getSubBox(box);
            var handle = box.getElementsByClassName('handleIcon');
            if(state=='open'){
                clickArea.setAttribute('state','close');
                subBox.style.display = 'none';
                handle[0].style.display = 'inline';
                handle[1].style.display = 'none';

                console.log('result'+'1');
            }else if(state=='close'){
                clickArea.setAttribute('state','open');
                subBox.style.display = 'block';
                handle[1].style.display = 'inline';
                handle[0].style.display = 'none';

                console.log('result'+'2');
            }else{
                console.log('result'+'3')
            }
        },
        showSubItem:function(box){
            if(!isNaN(box)){
                box = nav.getBoxes()[parseInt(box,10)];
            }
            box.setAttribute('state','show');
            return box;
        },
        clickBind:function(){
            var boxes = nav.getBoxes();
            function bind(tmp){
                nav.getClickArea(boxes[i]).onclick = function(e){
                    var state = nav.getClickArea(boxes[tmp]).getAttribute(nav.stateName);
                    e.preventDefault();
                    nav.stateChange(state,tmp);
                }
            }
            for(var i=0;i<boxes.length;i++){
                bind(i);
            }
        }
    };
    nav.clickBind();
    return nav;
});
//console.log(checkBox.handle())