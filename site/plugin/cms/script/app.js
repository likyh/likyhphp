/**
 * Created by linyh on 2015/7/25.
 */
var requireName={
    awesome:'css!../../awesome/font-awesome.css',
}
require.config({
    baseUrl: 'plugin/cms/script',
    paths: {
        jquery: '../../jquery-1',
        style: '../style',
        plugin: '../..',
        domReady:'../../require/domReady'
    },
    map: {
        '*': {
            'css': 'plugin/require/css'
        }
    },
    shim : {
        'util': ['css!../style/1.css'],
        'plugin/ueditor/ueditor.all': ["plugin/ueditor/ueditor.config"],
    }
});

require(["common"],function(){
});