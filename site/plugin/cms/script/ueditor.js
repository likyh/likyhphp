/**
 * Created by linyh on 2015/7/25.
 */
define(["jquery","plugin/ueditor/ueditor.all"],function($){
    $(".likyh-module.ueditor>script.ueditor").each(function(){
        var ue = UE.getEditor($(this).attr("id"));
        ue.ready(function() {
            var html = ue.getContent();
            ue.execCommand('pasteplain');
        });
    });
});