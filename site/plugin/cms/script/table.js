/**
 * Created by linyh on 2015/7/25.
 */
//表格
define(["jquery","css!style/table.css"],function($){
    return {
        box : document.getElementById('dataTable'),
        allChecks:function(){
            return checkBox.box.querySelectorAll("tbody input[type='checkbox']");
        },
        handle:function(){
            return checkBox.box.querySelector("thead input[type='checkbox']");
        },
        change:function(){
            var handle = checkBox.handle();
            var checks = checkBox.allChecks();
            if(!handle.checked){
                for(var i=0;i<checks.length;i++){
                    checks[i].checked = false;
                }
            }else{
                for(var i=0;i<checks.length;i++){
                    checks[i].checked = true;
                }
            }
        },
        init:function(){
            var handle = checkBox.handle();
            handle.onchange = function(){
                checkBox.change()
            }
        }
    }
});