<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <base href="<?php echo $system['siteRoot'];?>" />
    <script>root='<?php echo $system['root'];?>';</script>
    <title><?php echo $r['pageTitle'];?> - <?php echo $r['copyright']['title'] ?></title>
    <link href="plugin/cms/style/common.css" rel="stylesheet"/>
    <link href="plugin/awesome/font-awesome.min.css" rel="stylesheet"/>
    <script src="plugin/require.js" defer async="async" data-main="plugin/cms/script/app.js"></script>
</head>
<body>
<div id="likyh_container">
    <header>
        <div id="company">
            <div class="title <?php echo $r['copyright']['logo']?"show":"hidden";?>"><span>@LikyhCMS</span></div>
        </div>
        <div id="logo"><?php echo $r['copyright']['title'] ?></div>
        <?php echo $r['userInfoWidget']; ?>
    </header>
    <div id="main">
        <?php echo $r['navWidget']; ?>
        <div id="likyh_content">
            <div class="content_inner">
                <div class="contentTitle">
                    <span>
                        <em class="icon-sitemap"></em>
                        <a href="javascript:">后台首页</a>
                        <?php if(!empty($r['pageTitle'])){ ?><a href="javascript:"><?php echo $r['pageTitle'];?></a><?php } ?>
                        <?php if(!empty($r['actionTitle'])){ ?><a href="javascript:"><?php echo $r['actionTitle'];?></a><?php } ?>
                    </span>
                </div>
                <?php echo $r['controlWidget']; ?>
                <div class="likyh-module <?php echo $r['likyhModuleName']; ?>" data-likyh-module="<?php echo $r['likyhModuleName']; ?>">
                    <?php import_tpl($r['sourceTpl']);?>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div id="siteMap">
            <ul>
                <li><a href="http://code.likyh.com">git on likyh</a></li>
                <li><a href="http://www.likyh.com">关于我们</a></li>
                <li><a href="mailto:l@likyh.com">意见反馈</a></li>
                <li><script src="http://s11.cnzz.com/z_stat.php?id=1254053139&web_id=1254053139" language="JavaScript"></script></li>
            </ul>
        </div>
        <div id="copyright">
            <p>Created Via LikyhStudio, Welcome to connact us!</p>
        </div>
    </footer>
</div>
</body>
</html>