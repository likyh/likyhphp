<?php if(!empty($r['condition'])): ?>
<form action="<?php e_qus();?>" method="get">
    <fieldset>
        <legend>属性筛选</legend>
        <?php foreach($r['condition'] as $field=>$currentValue){ $config=$r['fieldArray'][$field]; ?>
            <?php if($config['search']==CodeCms::FORM_SELECT){ ?>
                <label for="<?=$field?>Input"><?=$config['name']?></label><br/>
                <select name="condition[<?=$field?>]" id="<?=$field?>Input">
                    <option value=''>所有</option>
                    <?php foreach($config['map'] as $k=>$v){?>
                        <option value="<?=trim($k)?>" <?php if($k==$currentValue) echo 'selected'?>><?=$v?></option>
                    <?php }?>
                </select>
            <?php }elseif($config['search']==CodeCms::FORM_TEXT){ ?>
                <label for="<?=$field?>Input"><?=$config['name']?></label><br/>
                <input type="text" name="condition[<?=$field?>]" id="<?=$field?>Input" value="<?=$r['condition'][$field]?>" placeholder="请输入<?=$config['name']?>"><br/>
            <?php }elseif($config['search']==CodeCms::FORM_NONE){
            } ?>
        <?php } ?>
        <input type="submit" value="搜 索"/><br/>
        <a href="<?php e_qus() ?>">清空所有筛选内容</a>
    </fieldset>
</form>
<?php endif; ?>
<?php if($result['sortArray']): ?>
<div class="sortDiv">
    <?php $up='fa fa-long-arrow-down'; $down='fa fa-long-arrow-up'; ?>
    <a href="<?php e_qus(array("condition"=>$r['condition'])+array("sortArray"=>$r['defaultSort'])) ?>">综合排序</a>
    <?php foreach($result['sortArray'] as $field=>$config){ $config=$r['fieldArray'][$field]?>
        <?php if($config['sort']&CodeCms::SORT_EXIST_UNIQUE==0){ ?>
        <a href="<?php e_qus(array("condition"=>$r['condition'])+array("sortArray"=>array($field=>!$r['sortArray'][$field]))) ?>">
            <?=$config['name']?><i class="<?=$r['sortArray'][$field]? $down: $up?>"></i></a>
        <?php }elseif($config['sort']&CodeCms::SORT_EXIST_UNIQUE>0){ ?>
            <a href="<?php e_qus(array("condition"=>$r['condition'])+array("sortArray"=>array($field=>!$r['sortArray'][$field])+$r['sortArray'])) ?>">
            <?=$config['name']?><i class="<?=$r['sortArray'][$field]? $down: $up?>"></i></a>
        <?php }elseif($config['sort']&CodeCms::SORT_EXIST_SORT==0){
        } ?>
    <?php } ?>
</div>
<?php endif; ?>
<table id="dataTable" class="dataTable">
    <thead>
    <tr>
        <?php foreach($r['showField'] as $v){ ?>
        <th><?=$r['fieldArray'][$v]['name']?></th>
        <?php } ?>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($r['data'] as $data){?>
    <tr>
        <?php foreach($r['showField'] as $field){ $config=$r['fieldArray'][$field]; ?>
            <td><?php
            if($config['table']==CodeCms::TABLE_MAP){
                echo isset($config['map'][$data[$field]])? $config['map'][$data[$field]]: "未知";
            }elseif($config['table']==CodeCms::TABLE_DATE){
                echo !empty($data[$field])? date("Y-m-d",strtotime($data[$field])): "";
            }elseif($config['table']==CodeCms::TYPE_TIME){
                echo !empty($data[$field])? date("H:i:s",strtotime($data[$field])): "";
            }elseif($config['table']==CodeCms::TABLE_DATETIME){
                echo !empty($data[$field])? date("Y-m-d H:i:s",strtotime($data[$field])): "";
            }elseif($config['table']==CodeCms::TYPE_TEXT){
                echo "<a href='javascript:alert(\"{$data[$field]}\");'>显示</a>";
            }elseif($config['table']==CodeCms::TABLE_CUSTOM){
                echo call_user_func($config['showFunction'],$data,$config);
            }elseif($config['table']==CodeCms::TABLE_MODE){
                $mode=$config['mode'];
                $modeData=call_user_func(array($mode,"getDetail"),$data[$field]);
                echo call_user_func($config['showFunction'],$modeData,$config);
            }elseif($config['table']==CodeCms::TABLE_NONE){
            }else{
                echo $data[$field];
            } ?></td>
        <?php } ?>
        <td>
            <a href="<?php e_action($r['url']['modify'],array("id"=>$data['id'])); ?>">编辑</a>
            <a href="<?php e_action($r['url']['delete'],array("id"=>$data['id'])); ?>">删除</a>
        </td>
    </tr>
    <?php } ?>
    </tbody>
</table>
<?php echo $result['page']; ?>