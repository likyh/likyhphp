<form action="<?=$r['url']['submitUrl'];?>" method="post" <?php if($r['multipart']) echo 'enctype="multipart/form-data"';?>>
    <?php foreach($r['modifyGroup'] as $groupKey=>$fieldArray){ ?>
        <?php if(!empty($groupKey)){ ?><fieldset><legend><?=$r['pageConfig']['modify_group_name'][$groupKey]?></legend><?php } ?>
        <?php foreach($fieldArray as $field){ $config=$r['fieldArray'][$field]; ?>
            <?php if($config['modify']==CodeCms::FORM_SELECT){ ?>
                <label for="<?=$field?>Input"><?=$config['name']?></label>
                <select name="data[<?=$field?>]" id="<?=$field?>Input">
                    <?php foreach($config['map'] as $k=>$v){
                        if(!is_array($v)){ ?>
                            <option value="<?=trim($k)?>" <?php if(isset($r['data'][$field])&&$k==$r['data'][$field]) echo 'selected'?>><?=$v?></option>
                        <?php }else{ ?>
                            <optgroup label="<?=$k?>">
                                <?php foreach($v as $subK=>$subV){ ?>
                                    <option value="<?=$subK?>" <?php if(isset($r['data'][$field])&&$subK==$r['data'][$field]) echo 'selected'?>><?=$subV?></option>
                                <?php } ?>
                            </optgroup>
                        <?php } ?>
                    <?php }?>
                </select>
                <br/>
            <?php }elseif(in_array($config['modify'],array(CodeCms::FORM_TEXT,CodeCms::FORM_DATETIME,CodeCms::FORM_TIME,CodeCms::FORM_TEXT,))){ ?>
                <label for="<?=$field?>Input"><?=$config['name']?></label>
                <input type="<?=$config['modify']?>" name="data[<?=$field?>]" id="<?=$field?>Input" value="<?=isset($r['data'][$field])?$r['data'][$field]:''?>" placeholder="请输入<?=$config['name']?>"/>
                <br/>
            <?php }elseif($config['modify']==CodeCms::FORM_TEXTAREA){?>
                <label for="<?=$field?>Input"><?=$config['name']?></label>
                <textarea name="data[<?=$field?>]" id="<?=$field?>Input" placeholder="请输入<?=$config['name']?>"><?=isset($r['data'][$field])?$r['data'][$field]:''?></textarea>
                <br/>
            <?php }elseif($config['modify']==CodeCms::FORM_DATE){?>
                <label for="<?=$field?>Input"><?=$config['name']?></label>
                <input type="date" name="data[<?=$field?>]" id="<?=$field?>Input" value="<?=isset($r['data'][$field])?date("Y-m-d",strtotime($r['data'][$field])):''?>" placeholder="请输入<?=$config['name']?>"/>
                <br/>
            <?php }elseif($config['modify']==CodeCms::FORM_FILE){ ?>
                <label for="<?=$field?>Input"><?=$config['name']?></label>
                <input type="<?=$config['modify']?>" name="<?=$field?>" id="<?=$field?>Input" value="<?=isset($r['data'][$field])?$r['data'][$field]:''?>" placeholder="请输入<?=$config['name']?>"/>
                <br/>
            <?php }elseif($config['modify']==CodeCms::FORM_UEDITOR){ ?>
            <div class="likyh-module ueditor" data-likyh-module="ueditor">
                <script>
                    ueditorController='<?php e_url("admin","ueditor","index");?>';
                </script>
                <script name="data[<?=$field?>]" id="<?=$field?>Input" class="ueditor" type="text/plain"><?=isset($r['data'][$field])?$r['data'][$field]:''?></script>
            </div>
            <?php }elseif($config['modify']==CodeCms::FORM_HIDDEN){ ?>
                <input type="hidden" name="data[<?=$field?>]" value="<?=isset($r['data'][$field])?$r['data'][$field]:''?>"/>
            <?php }elseif($config['modify']==CodeCms::FORM_NONE){
            } ?>
        <?php } ?>
        <?php if(!empty($groupKey)){ ?></fieldset><?php } ?>
    <?php } ?>
    <input type="submit"/>
</form>
