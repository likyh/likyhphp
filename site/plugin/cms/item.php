<?php foreach($r['item'] as $v){ ?>
<div class="item-item" style="background-color: <?php echo $v['color'] ?>;">
    <a href="<?php echo $v['url'];?>">
        <div class="icon"><i class="fa <?php echo $v['icon'] ?>"></i></div>
        <div class="name"><?php echo $v['name'] ?></div>
    </a>
</div>
<?php } ?>