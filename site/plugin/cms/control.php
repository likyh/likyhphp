<div id="likyhContentControl">
<?php foreach($r['item'] as $k => $i){ ?>
    <a href="<?php echo $i['url'];?>" class="button control" id="<?php echo $k;?>ContentBtn">
        <div class="icon"><span class="fa <?php echo $i['icon']; ?>"></span></div>
        <div class="title"><?php echo $i['name']; ?></div>
    </a>
<?php } ?>
</div>