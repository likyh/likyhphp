<nav>
    <h2><?php echo $r['title'];?></h2>
    <?php foreach($r['item'] as $i){ ?>
    <div class="menuItem">
        <a class="title" href="<?php if(isset($i['url'])){echo $i['url'];}else{echo "javascript:";} ?>" state="<?php if(isset($i['item'])&&!empty($i['item'])){echo "close";}else{echo "empty";} ?>">
            <div class="icon "><span class="fa <?php echo $i['icon']; ?>"></span></div>
            <div class="text"><?php echo $i['name']; ?></div>
            <?php if(isset($i['item'])&&!empty($i['item'])){ ?>
            <div class="handle">
                <i class="open handleIcon fa fa-angle-up"></i>
                <i class="close handleIcon fa fa-angle-down"></i>
            </div>
            <?php } ?>
        </a>
       
        <?php if(isset($i['item'])&&!empty($i['item'])){ ?>
        <div class="subItemBox">
            <?php foreach ($i['item'] as $j) { ?>
            <div class="subItem">
                <a class="title" href="<?php echo $j['url']; ?>">
                    <div class="icon "><span class="fa <?php echo $j['icon']; ?>"></span></div>
                    <div class="text"><?php echo $j['name']; ?></div>
                </a>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    <?php } ?>
</nav>