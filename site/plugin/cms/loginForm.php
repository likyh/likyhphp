<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="renderer" content="webkit"/>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <title>欢迎登陆后台</title>
    <base href="<?php echo $system['siteRoot'];?>" />
    <link href="plugin/normalize.min.css" rel="stylesheet"/>
	<link href="plugin/cms/style/loginForm.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <div class="logo"></div>
    <form action="<?php echo $r['actionUrl'];?>" method="post">
        <fieldset>
            <input type="text" name="<?php echo $r['userTag'];?>" id="userInput" placeholder="请输入用户名"><br>
            <input type="password" name="<?php echo $r['passTag'];?>" id="passwordInput" placeholder="输入密码"><br>
            <input type="submit" value="登陆">
        </fieldset>
    </form>
</div>
</body>
</html>