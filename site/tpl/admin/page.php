<table>
    <thead>
    <tr>
        <th>id</th>
        <th>标题</th>
        <th>作者</th>
        <th>时间</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>新闻1</td>
        <td>邓松</td>
        <td>2014-3-26</td>
    </tr>
    <tr>
        <td>2</td>
        <td>新闻2</td>
        <td>邓松2</td>
        <td>2014-3-26</td>
    </tr>
    <tr>
        <td>3</td>
        <td>新闻3</td>
        <td>邓松3</td>
        <td>2014-3-26</td>
    </tr>
    <tr>
        <td>4</td>
        <td>新闻4</td>
        <td>邓松4</td>
        <td>2014-3-26</td>
    </tr>
    <tr>
        <td>1</td>
        <td>新闻1</td>
        <td>邓松</td>
        <td>2014-3-26</td>
    </tr>
    <tr>
        <td>2</td>
        <td>新闻2</td>
        <td>邓松2</td>
        <td>2014-3-26</td>
    </tr>
    <tr>
        <td>3</td>
        <td>新闻3</td>
        <td>邓松3</td>
        <td>2014-3-26</td>
    </tr>
    <tr>
        <td>4</td>
        <td>新闻4</td>
        <td>邓松4</td>
        <td>2014-3-26</td>
    </tr>
    </tbody>
</table>
<?php
/** @var PageWidget */
$page=$r['page'];
?>
<div class="dataPage">
    <ul>
        <li>共<span><?php echo $page->getLast();?></span>页/
            <span><?php echo $page->getTotal();?></span>条记录
            当前是第<span><?php echo $page->getCurrentPage();?></span>页</li>
        <li><a href="<?php echo $page->getPageUrl($page->getFirst());?>">首页</a></li>
        <?php if($page->ifPrev()) { ?>
            <li><a href="<?php echo $page->getPageUrl($page->getPrev());?>">上一页</a></li>
        <?php }?>
        <?php foreach($page as $i){ ?>
        <li class="page"><a href="<?php echo $page->getPageUrl($i);?>"><?php echo $i;?></a></li>
        <?php } ?>
        <?php if($page->ifNext()) { ?>
            <li><a href="<?php echo $page->getPageUrl($page->getNext());?>">下一页</a></li>
        <?php }?>
        <li><a href="<?php echo $page->getPageUrl($page->getLast())?>">末页</a></li>
    </ul>
</div>
当然，也可以使用传统的方法生成url，<?php e_qus("total={$page->getTotal()}&page={$page->getFirst()}");?>
<br/>
当然，最简单的还是向下面这样：
<?php echo $r['page']; ?>