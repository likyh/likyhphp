<form action="#" method="post">
    <fieldset>
        <legend>表单组1</legend>
        <label for="i1">请输入你的姓名</label>
        <input type="text" name="i1" id="i1" placeholder="hahah"/>
        <br/>
        <label for="i2">你的邮箱</label>
        <input type="email" name="i2" id="i2" value="123"/>
        <br/>
        <label for="i3">地址</label>
        <input type="url" name="i3" id="i3" value="123"/>
        <br/>
        <label for="i4">密码</label>
        <input type="password" name="i4" id="i4" value="123"/>
        <br/>
        <label for="time">时间</label>
        <input type="date" name="time" id="time" value=""/>
        <br/>
        <label for="i13">i1</label>
        <textarea id="i13">12312313</textarea>
        <br/>
        <label for="price">价格</label>
        在每月
        <input type="text" class="mini" name="price-date" id="price-date" value=""/>号
        <input type="text" class="mini" name="price" id="price" value=""/>折优惠
        <textarea id="price">12312313</textarea>
    </fieldset>
    <fieldset class="radio">
        <legend>表单组2 单选组</legend>
        <input type="radio" name="radio" id="radio_1"/>
        <label for="radio_1">i1</label>
        <input type="radio" name="radio" id="radio_2"/>
        <label for="radio_2">i2</label>
        <input type="radio" name="radio" id="radio_3"/>
        <label for="radio_3">i3</label>
        <input type="radio" name="radio" id="radio_4"/>
        <label for="radio_4">i1</label>
    </fieldset>
    <fieldset class="checkbox">
        <legend>表单组3 多选组</legend>
        <input type="checkbox" name="checkbox" id="checkbox_1" value="a"/>
        <label for="checkbox_1">checkbox_1</label>
        <input type="checkbox" name="checkbox" id="checkbox_2" value="b"/>
        <label for="checkbox_2">checkbox_2</label>
        <input type="checkbox" name="checkbox" id="checkbox_3" value="c"/>
        <label for="checkbox_3">checkbox_3</label>
        <input type="checkbox" name="checkbox" id="checkbox_4" value="d"/>
        <label for="checkbox_4">checkbox_4</label>
    </fieldset>
    <fieldset>
        <legend>表单组4 开关</legend>
        <span>是否打开WiFi</span>
        <div class="checkboxSwicher">
        <input type="checkbox" class="switcher" name="switcher" id="switcher" value="1"/>
        <label for="switcher"></label>
        </div>
    </fieldset>
    <fieldset>
        <legend>表单组5 下拉菜单</legend>
        <label for="type">所属分类</label>
        <select name="select" class="mini" id="type">
            <option>依然</option>
            <option>阳辉</option>
            <option>松高</option>
        </select>
        <select name="select2" class="mini" id="type2">
            <option>依然</option>
            <option>阳辉</option>
            <option>松高</option>
        </select>
        <select name="select3" class="mini" id="type3">
            <option>依然</option>
            <option>阳辉</option>
            <option>松高</option>
        </select>
        <br/>

        <label for="select4">普通下拉菜单</label>
        <select name="select4" id="select4">
            <option>徐州盛世光明软件技术有限公司</option>
            <option>徐州盛世光明软件技术有限公司</option>
            <option>徐州盛世光明软件技术有限公司</option>
        </select>
        <br/>

        <label for="company">公司名称</label>
        <select name="company" class="long" id="company">
            <option>超长超长超长超长超长超长超长超长超长超长超长超长超长超长超长超长的选项1</option>
            <option>超长超长超长超长超长超长超长超长超长超长超长超长超长超长超长超长的选项2</option>
            <option>超长超长超长超长超长超长超长超长超长超长超长超长超长超长超长超长的选项3</option>
        </select>
        <br/>
    </fieldset>
    <fieldset>
        <legend>单图上传</legend>
        这个就要是那种一个上传表单，下面一个图片可以预览的
        <div class="likyhModule singlePic" data-likyh-module="singlePic">
            <input type="file" class=""/>
            <img src="#" alt=""/>
        </div>
    </fieldset>
    <fieldset>
        <legend>多图上传组件，暂时不做</legend>
        因为还没想好怎么做
    </fieldset>
    <fieldset class="multiInput">
        <legend>产品规格</legend>
        <table class="">
            <thead>
            <tr>
                <th>规格名称</th>
                <th>库存余量</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><input type="text" name="multi[0][name]" required></td>
                <td><input type="text" name="multi[0][stock]" required></td>
                <td><a href="javascript:" class="delete"><i class="fa fa-close"></i></a></td>
            </tr>
            </tbody>
        </table>
        <a href="javascript:" class="add">添加一行</a>
    </fieldset>
    <fieldset class="ueditor">
        <legend>富文本</legend>
        <script id="aTextInput" name="aTextInput" class="editor" type="text/plain"></script>
    </fieldset>
    <input type="submit"/>
    <a href="javascript:" class="go_back">返回</a>
</form>