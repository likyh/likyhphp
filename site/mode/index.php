<!DOCTYPE html>
<html lang="cn">
<head>
	<meta charset="utf-8" />
    <base href="<?php echo $system['siteRoot'];?>" />
    <script type="text/javascript">
        root='<?php echo $system['root'];?>';
    </script>
    <script data-main="script/coach" src="plugin/avalon/avalon.mobile.min.js"></script>
    <script data-main="script/coach" src="mode/filter.js"></script>
    <script data-main="script/coach" src="mode/mode.js"></script>

    <title>MySQL数据库Mode生成工具</title>
    <style>
        * {
            font-family: Consolas, Inconsolata, Monaco, Menlo,  "Courier New", monospace;
        }
        #confirmCode {
            width: 90%;
            height: 500px;
            margin: 5%;
        }
    </style>
</head>

<body>
<h1>MySQL数据库Mode生成工具</h1>
<div class="db" ms-controller="db" ms-visible="ready">
    <h2>选择数据库</h2>
    <span ms-repeat="list">
        <a ms-click="loadTable(el)" href="javascript:">{{el}}</a>
    </span>
</div>
<div class="table" ms-controller="table" ms-visible="ready">
    <h2>选择数据表</h2>
    <span ms-repeat="list">
        <a ms-click="loadColumn(el)" href="javascript:">{{el}}</a>
    </span>
</div>
<div class="column" ms-controller="column" ms-visible="ready">
    <h2>选择选择模板</h2>
    <div class="func" ms-visible="codeChangeReady">
        <div ms-if="func.funcDelete" ms-include-src="'mode/get.html'" data-include-replace="true"></div>
        <div ms-if="func.funcCreate" ms-include-src="'mode/detail.html'" data-include-replace="true"></div>
        <div ms-if="func.funcCreate" ms-include-src="'mode/create.html'" data-include-replace="true"></div>
        <div ms-if="func.funcDelete" ms-include-src="'mode/update.html'" data-include-replace="true"></div>
        <div ms-if="func.funcDelete" ms-include-src="'mode/delete.html'" data-include-replace="true"></div>
    </div>
    <hr/>
    <form action="<?php e_action("download"); ?>" method="post" target="_blank">
        <a href="javascript:" ms-click="codeUpdate">1. 生成</a>
        <a href="javascript:" ms-click="codeConfirm">2. 修改</a>
        <input type="submit" value="3. 下载"/>
        <pre id="codeUpdate" ms-include-src="'mode/base.html'" ms-if="codeUpdateReady"></pre>
        <input type="text" name="title" ms-if="codeConfirmReady" ms-attr-value="avalon.filters.varCamel(table)+'Mode.class.php'"/>
        <textarea name="code" id="confirmCode" ms-duplex="code" ms-if="codeConfirmReady"></textarea>
    </form>
</div>
</body>
</html>