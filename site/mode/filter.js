avalon.filters.varCamel=function(varName) {
    var strs=varName.split("_");
    varName='';
    for(var i in strs){
        var s=strs[i];
        varName+= likyhString.firstUpper(s.toLowerCase());
    }
    return varName;
};
avalon.filters.varVar=function(varName) {
    return "$"+likyhString.firstLower(varName);
};
avalon.filters.varFormat=function(varName,info){
    var s="";
    switch(info.class){
        case 'time':
            s='date("Y-m-d H:i:s")';
            break;
        case 'pri':
        case 'idJoin':
            s='(int)'+varName+"";
            break;
        case 'idsJoin':
            s='is_array('+varName+')?explodeDeal(",",'+varName+',function(s){return (int)s;}):'+varName+'';
            break;
        case 'data':
            switch(info.classSub){
                case 'time':
                    s='date("Y-m-d H:i:s",strtotime('+varName+"))";
                    break;
                case 'number':
                    if(info.classExtra=="int"){
                        s='(int)'+varName+"";
                    }else{
                        s='(double)'+varName+"";
                    }
                    break;
                case 'text':
                    s='TextFilter::htmlCut('+varName+',0)';
                    break;
                case 'normal':
                    s='(string)'+varName+'';
                    break;
                default:
                    break;
            }
            break;
        default :
            s='(string)'+varName+"";
    }
    return s;
};
var likyhString={
    firstUpper:function(s){
        return s.substring(0,1).toUpperCase() + s.substring(1);
    },
    firstLower:function(s){
        return s.substring(0,1).toLowerCase() + s.substring(1);
    }
};