var dbModel=avalon.define({
    $id:"db",
    ready:false,
    list:[],
    loadTable:function(db){
        avalon.getJSON(root+'mode/table?db='+db, function(d) {
            d=JSON.parse(d);
            if(d && d.data){
                tableModel.list= d.data;
                tableModel.db=db;
                tableModel.ready=true;
            }
        })
    }
});
var tableModel=avalon.define({
    $id:"table",
    db:'',
    ready:false,
    list:[],
    loadColumn:function(table){
        avalon.getJSON(root+'mode/column?db='+tableModel.db+'&table='+table, function(d) {
            d=JSON.parse(d);
            if(d && d.data&& d.column){
                columnModel.list= d.data;
                columnModel.$fire("down!listChange", d.data);
                columnModel.db=tableModel.db;
                columnModel.table=table;
                columnModel.ready=true;
                columnModel.codeChangeReady=true;
                columnModel.codeUpdateReady=false;
                columnModel.codeConfirmReady=false;
            }
        });
        avalon.getJSON(root+'mode/dbInfo?db='+tableModel.db+'&table='+table, function(d) {
            d=JSON.parse(d);
            columnModel.tableComment= d.comment;
        });
    }
});
var columnModel=avalon.define({
    $id:"column",
    db:'',
    table:'',
    tableComment:'123',
    ready:false,
    tplName:'normal',
    column:{},
    list:[],
    func:{
        funcList:true,
        funcDetail:true,
        funcExist:false,
        funcCreate:true,
        funcUpdate:true,
        funcDelete:true
    },
    replaceLT:function(s){
        return s.replace("<","&lt;");
    },
    codeChangeReady:false,
    codeUpdateReady:false,
    codeConfirmReady:false,
    codeUpdate: function () {
        columnModel.codeChangeReady=false;
        columnModel.codeUpdateReady=true;
    },
    codeConfirm:function(){
        columnModel.codeConfirmReady=true;
        columnModel.code=document.querySelector("#codeUpdate").textContent.replace(/\n\n/g,"\n");
        columnModel.codeUpdateReady=false;
    },
    code:""
});

require(["./mmRequest.modern"], function(avalon) {
    avalonAjax = avalon.ajax;
    avalon.getJSON(root+'mode/db', function(d) {
        d=JSON.parse(d);
        if(d && d.data){
            dbModel.list= d.data;
            dbModel.ready=true;
        }
    });
});