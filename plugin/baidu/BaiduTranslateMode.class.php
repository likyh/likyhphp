<?php
/**
 * Api Store 翻译
 * Class BaiduTranslateMode
 */
class BaiduTranslateMode extends Data{
    /** @return BaiduTranslateMode */
    public static function init() {
        return parent::init();
    }

    protected $config;
    protected function onStart() {
        $this->config=getConfig("baidu","translate",true);
    }

    public function translate($query = 'hello',$from = 'en',$to = 'zh'){
        $url=$this->config['url'];
        $param=array(
            "query" => $query,
            "from" => $from,
            "to" => $to
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }
}