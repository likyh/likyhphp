<?php
/**
 * Api Store 手机归属地查询
 * Class BaiduMobileMode
 */
class BaiduMobileMode extends Data{
    /** @return BaiduMobileMode */
    public static function init() {
        return parent::init();
    }

    protected $config;
    protected function onStart() {
        $this->config=getConfig("baidu","mobile",true);
    }

    /**
     * 手机号码归属地查询
     * @param string $tel
     * @return bool|mixed
     */
    public function getAddress($tel){
        $url=$this->config['url'];
        $param=array(
            "tel" => $tel
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }
} 