<?php
/**
 * 从聚合获取数据
 * Class JuheWeatherMode
 */
class JuheWeatherMode extends Data {
    protected $appkey;
    protected $config;

    protected function onStart() {
        $this->config=getConfig("juhe","weather",true);
        $this->appkey=$this->config["appkey"];
    }

    public function getBase($city){
        $url=$this->config["index"];
        $param=array(
            "cityname"=>$city,
            "dtype" => "json",
            "key" => $this->appkey
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }

    public function getUni(){
        $url=$this->config["uni"];
        $param=array(
            "dtype" => "json",
            "key" => $this->appkey
        );
        $dataStr=Request::get($url, $param);
        if($dataStr!==false){
            return json_decode($dataStr,true);
        }else{
            return false;
        }
    }

    public function getForecast3h($city){
        $url=$this->config["forecast3h"];
        $param=array(
            "cityname"=>$city,
            "dtype" => "json",
            "key" => $this->appkey
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }
} 