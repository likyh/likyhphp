<?php
/**
 * sae平台上使用的key-value数据库，
 * 相比于session，可以长久保存
 */
class SaeKvDb implements CacheInterface {
    protected $seaKV;
    protected static $selfObject;
    protected $appInfo;

    /**
     * 初始化函数，并返回当前类
     * @return SaeKvDb
     */
    public static function init(){
        if(!self::$selfObject){
            self::$selfObject=new SaeKvDb();
        }
        return self::$selfObject;
    }
    protected function __construct(){
        $this->seaKV = new SaeKV();
        $this->seaKV->init();
    }


    public function check($key) {
        // TODO: Implement check() method.
    }
    public function get($keys){
        return is_array($keys)? $this->seaKV->mget($keys): $this->seaKV->get($keys);
    }
    /**
     * 通过前缀搜索值
     * @unused
     * @param $prefix_key Key值的前缀
     * @param int $count
     * @param string $start_key
     * @return mixed
     */
    public function getMulPrefix($prefix_key, $count, $start_key=''){
        return $this->seaKV->pkrget($prefix_key, $count, $start_key);
    }
    public function set($key,$value){
        return $this->seaKV->set($key,$value);
    }
    /** @unused */
    public function replace($key,$value){
        return $this->seaKV->replace($key, $value);
    }
    /** @unused */
    public function add($key,$value){
        return $this->seaKV->add($key,$value);
    }
    public function delete($key){
        return $this->seaKV->delete($key);
    }

    public function offsetExists($offset){
        return $this->seaKV->get($offset);
    }
    public function offsetGet($offset){
        return $this->seaKV->get($offset);
    }
    public function offsetSet($offset, $value){
        $this->seaKV->set($offset, $value);
    }
    public function offsetUnset($offset){
        return $this->seaKV->delete($offset);
    }
}