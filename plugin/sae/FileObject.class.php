<?PHP
/**
* 文件处理统一接口类
*/
class FileObject {
    /**
     * 随机生成文件名
     * @param $ext 文件扩展名
     * @return string
     */
    public static function fileNameByRandom($ext=null){
        if(empty($ext)) $ext="tmp";
        $randomStr=getHash(rand(1,30000).time().$ext.rand(1,30000));
        return $randomStr.".".$ext;
    }
    /**
     * 使用时间生成文件名
     * @param $ext 文件扩展名
     * @return string
     */
    public static function fileNameByTime($ext=null){
        if(empty($ext)) $ext="tmp";
        return date("Hi_").rand(100000000, 1000000000).'.'.$ext;
    }

    /**
     * 获取文件扩展名
     * @param $filename
     * @return string
     */
    public static function fileExt($filename){
        $posOfDot=strripos($filename, '.');
        if($posOfDot!==false){
            return strtolower(substr($filename,$posOfDot+1,strlen($filename)));
        }
        return false;
    }

    /**
     * 读取一个文件
     * @param string $filename 文件名
     * @return string
     */
    public static function get($filename){
        $storage=new SaeStorage();
        $tmp=explode("/",$filename);
        $domain = array_shift($tmp);
        $destFileName = implode("/",$tmp). $filename;
        return $storage->read($domain,$destFileName);
    }

    /**
     * 保存一个上传来的文件
     * @param string $path 文件目录
     * @param string $filename 文件名
     * @param string $sourceFile 临时文件文件名
     * @return array(state:bool, path:string, filename:string) 返回格式参照createByContent
     */
    public static function createByTmp($path, $filename, $sourceFile){
        $storage=new SaeStorage();
        $tmp=explode("/",$path);
        $domain = array_shift($tmp);
        $destFileName = implode("/",$tmp). $filename;

        $result = $storage->upload($domain,$destFileName, $sourceFile);
        if(!empty($result)){
            return array("success",null,null,$result);
        }
        return array("文件保存时出错",null,null,null);
    }

    /**
     * 复制一个文件
     * @param string $path 文件目录
     * @param string $filename 文件名
     * @param string $sourceFile 源文件文件名
     * @return array(state:bool, path:string, filename:string) 返回格式参照createByContent
     */
    public static function createByCopy($path, $filename, $sourceFile){
        $storage=new SaeStorage();
        $tmp=explode("/",$path);
        $domain = array_shift($tmp);
        $destFileName = implode("/",$tmp). $filename;

        $result = $storage->upload($domain,$destFileName, $sourceFile);
        if(!empty($result)){
            return array("success",null,null,$result);
        }
        return array("文件保存时出错",null,null,null);
    }

    /**
     * 保存一个上传来的文件，文件名及子目录自动处理
     * @param string $path 文件目录
     * @param $ext 文件扩展名
     * @param string $sourceFile 临时文件文件名
     * @return array(state:bool, filename:string) 返回格式参照createByContent
     */
    public static function autoCreateByTmp($path, $ext=null, $sourceFile){
        $path=DirObject::getFolderAppendDate($path);
        return self::createByTmp($path, self::fileNameByTime($ext), $sourceFile);
    }

    /**
     * 复制一个文件，文件名及子目录自动处理
     * @param string $path 文件目录
     * @param string $ext 文件扩展名
     * @param string $sourceFile 源文件文件名
     * @return array(state:bool, filename:string) 返回格式参照createByContent
     */
    public static function autoCreateByCopy($path, $ext=null, $sourceFile){
        $path=DirObject::getFolderAppendDate($path);
        return self::createByCopy($path, self::fileNameByTime($ext), $sourceFile);
    }
}