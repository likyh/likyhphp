<?php
/**
 * CmsView 用于显示cms
 * @author linyh
 */
class CmsView {
    // cms配置信息
    protected $config;
    // 一系列功能的标题（对应url的page）
    protected $pageTitle;
    // 一个页面的标题（对应url的action）
    protected $actionTitle;

    public $navWidget;
    public $controlWidget;
    public $userInfoWidget;

    protected static $selfObject;
    protected function __construct() {
        $this->config=getConfig('cms',null);
        $this->config['controlFile']=null;

        $this->navWidget=new CmsNavWidget($this->config['navFile']);
        $this->controlWidget=new CmsControlWidget($this->config['controlFile']);
        $this->userInfoWidget=new CmsUserInfoWidget($this->config['userInfoFile']);

        CmsWidget::openLib($this->config['itemLibFile']);
    }

    /**
     * 初始化函数
     * @param string $pageTitle page标题（一系列页面标题）
     * @param string $controlFile 控制文件
     * @param array $config
     * @return CmsView
     */
    public static function init($pageTitle=null, $controlFile=null, $config=null){
        if(!isset(self::$selfObject)){
            self::$selfObject=new CmsView();
        }
        $cmsView=self::$selfObject;
        if($cmsView instanceof CmsView){
            if(!empty($pageTitle)){
                $cmsView->setPageTitle($pageTitle);
            }
            if(!empty($controlFile)){
                $cmsView->setControlFile($controlFile);
            }
            if(!empty($config)){
                $cmsView->loadConfig($config);
            }
        }
        return $cmsView;
    }

    public function loadConfig($config){
        if(is_array($config)){
            $this->config=array_merge($this->config,$config);
        }
    }

    // 设置各种属性
    public function setPageTitle($pageTitle) {
        $this->pageTitle = $pageTitle;
    }
    public function setActionTitle($actionTitle) {
        $this->actionTitle = $actionTitle;
    }
    public function setUserName($userName) {
        $this->userInfoWidget->setName($userName);
    }
    public function setControlFile($filename){
        $this->controlWidget->reopen($filename);
    }

    public function loginScene($actionUrl, $userTag='user',$passTag='pass'){
        $result=array(
            'actionUrl'=>$actionUrl,
            'userTag'=>$userTag,
            'passTag'=>$passTag
        );
        View::displayAsHtml($result,'plugin/cms/loginForm.php');
    }

    public function normalScene($result, $tpl, $likyhModuleName=""){
        $result['pageTitle']=$this->pageTitle;
        $result['actionTitle']=$this->actionTitle;
        $result['copyright']=array(
            "logo"=>$this->config['logoCopyright'],
            "bottom"=>$this->config['bottomCopyright'],
            "title"=>$this->config['copyrightTitle'],
        );

        $result['likyhModuleName']=$likyhModuleName;

        $result['navWidget']=$this->navWidget;
        $result['controlWidget']=$this->controlWidget;
        $result['userInfoWidget']=$this->userInfoWidget;

        $result['sourceTpl']=$tpl;
        View::displayAsHtml($result, "plugin/cms/tpl.php");
    }

    public function formScene($result, $tpl){
        $this->normalScene($result, $tpl, "form");
    }
    public function tableScene($result, $tpl){
        $this->normalScene($result, $tpl, "table");
    }
    // 展示一个选择项 **实验方法**
    public function itemScene($file){
        $info=CmsWidget::openJson($file);
        $result['title']=$info['title'];
        $result['item']=$info['item'];
        foreach($result['item'] as &$v){
            $v['url']=CmsWidget::urlFormat($v);
        }
        $this->normalScene($result, "plugin/cms/item.php", "item");
    }
    public function codeFormScene($result){
        $this->formScene($result,"plugin/cms/form.php");
    }
    public function codeTableScene($result){
        $this->tableScene($result,"plugin/cms/table.php");
    }
}
abstract class CmsWidget extends Widget {
    protected $control;
    protected $jsonData;
    protected $cacheHtml;
    // item lib 库
    public static $itemLib;

    function __construct($filename) {
        $this->jsonData=self::openJson($filename);
    }
    function reopen($filename){
        $this->jsonData=self::openJson($filename);
    }
    public static function openLib($filename){
        self::$itemLib=self::openJson($filename);
    }

    // 通用function，打开一个JSON文件
    public static function openJson($filename){
        $filename="./site/".$filename;
        return is_file($filename)&&is_readable($filename)? json(SimpleFile::read($filename)): array();
    }
    public static function urlFormat($array){
        $webRouter=WebRouter::init();
        // 生成url
        if(isset($array['absolute'])){
            $url=$webRouter->getAbsolute($array['absolute']);
        }else if(isset($array['url'])){
            $url=$array['url'];
        }else{
            $array+=array("set"=>null,"page"=>null,"action"=>null,"qus"=>null);
            $url=$webRouter->getURL($array['set'],$array['page'],$array['action'],$array['qus']);
        }
        return $url;
    }
    // 生成列表项
    protected static function itemFormat(&$array){
        foreach($array as &$v){
            if(!is_array($v)){
                $v=isset(self::$itemLib[$v])? self::$itemLib[$v]: array();
            }
            if(isset($v['lib'])&& isset(self::$itemLib[$v['lib']])){
                $v+=self::$itemLib[$v['lib']];
                unset($v['lib']);
            }
            if(!isset($v['icon'])){
                $v['icon']="fa-lock";
            }
            // 生成子菜单
            if(isset($v['item'])&&is_array($v['item'])&& !empty($v['item'])){
                self::itemFormat($v['item']);
            }
            $v['url']=self::urlFormat($v);
        }
        return $array;
    }
    public function addItemArray($value,$key=null){
        if(!empty($key)){
            $this->jsonData['item'][$key]=$value;
        }else{
            $this->jsonData['item'][]=$value;
        }
    }
    public function addItem($key, $icon, $name, $page=null, $action=null, $item=array(),$value=array()){
        $value+=array(
            "icon"=>$icon,
            "name"=>$name,
            "page"=>$page,
            "action"=>$action,
            "item"=>$item
        );
        $this->addItemArray($value,$key);
    }
    public function deleteItem($key){
        if(isset($this->jsonData['item'][$key])){
            unset($this->jsonData['item'][$key]);
        }
    }
    abstract function cal();
    function __toString() {
        if(!empty($this->jsonData)){
            return $this->cacheHtml?:$this->cal();
        }else{
            return '';
        }
    }
}
class CmsNavWidget extends CmsWidget{
    function cal(){
        $nav=$this->jsonData;
        $result['title']=$nav['title'];
        $result['item']=$this->itemFormat($nav['item']);
        return $this->cacheHtml=View::formatAsHtml($result,"plugin/cms/nav.php");
    }
}
class CmsControlWidget extends CmsWidget{
    function cal(){
        $info=$this->jsonData;
        $result['title']=isset($info['title'])? $info['title']: "控制菜单";
        $result['item']=$this->itemFormat($info['item']);
        return $this->cacheHtml=View::formatAsHtml($result,"plugin/cms/control.php");
    }
}
class CmsUserInfoWidget extends CmsWidget{
    protected $name;
    public function setName($name) {
        $this->name = $name;
    }
    function cal(){
        $info=$this->jsonData;
        $name=$this->name?:$info['defaultName'];
        $result['userTips']=sprintf($info['tips'],$name);
        $result['item']=$this->itemFormat($info['item']);
        return $this->cacheHtml=View::formatAsHtml($result,"plugin/cms/userinfo.php");
    }
}