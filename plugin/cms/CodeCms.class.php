<?php
/**
 * Created by PhpStorm.
 * User: linyh
 * Date: 2015/6/3
 * Time: 23:47
 */

abstract class CodeCms extends Activity {
    // 类型一般用数据库字段类型命名，这里最好还是不同类型一个名字的，数字值都一样，这么会减少很多错误
    const TYPE_VARCHAR='VARCHAR';
    const TYPE_PRIMARY='PRIMARY';
    const TYPE_CUSTOM='CUSTOM';
    const TYPE_FOREIGN='FOREIGN';
    const TYPE_TEXT='TEXT';
    const TYPE_ENUM='ENUM';
    const TYPE_TIMESTAMP='TIMESTAMP';
    const TYPE_TIME='TIME';
    const TYPE_DATE='DATE';
    const TYPE_URL='URL';

    // 显示在Table页面的方式
    const TABLE_NONE='NONE';
    const TABLE_STRING='STRING';
    const TABLE_CUSTOM='CUSTOM';
    const TABLE_MODE='MODE';
    const TABLE_TEXT='TEXT';
    const TABLE_MAP='MAP';
    const TABLE_DATETIME='DATETIME';
    const TABLE_TIME='TIME';
    const TABLE_DATE='DATE';
    const TABLE_URL='URL';

    // 显示在Form页面的方式，一般采用HTML表单的命名方式
    const FORM_NONE='NONE';
    const FORM_TEXT='TEXT';
    // TODO 这两个暂未实现
//    const FORM_CUSTOM='CUSTOM';
//    const FORM_MODE='MODE';
    const FORM_HIDDEN='HIDDEN';
    const FORM_TEXTAREA='TEXTAREA';
    const FORM_UEDITOR='UEDITOR';
    const FORM_SELECT='SELECT';
    const FORM_DATETIME='DATETIME';
    const FORM_TIME='TIME';
    const FORM_DATE='DATE';
    const FORM_FILE='FILE';

    // SORT_DEFAULT 可以和另外两个一起传入: SORT_DEFAULT|SORT_UNIQUE
    // 采用按位标示内容 第0位是否有搜索，第一位是否默认，第二位是UNIQUE(0)还是COMBINE(1)，第三位表示升降序
    const SORT_NONE=0x0;
    const SORT_DEFAULT=0x3;
    const SORT_UNIQUE=0x1;
    const SORT_COMBINE=0x5;
    const SORT_UP=0x9;
    const SORT_DOWN=0x1;
    const SORT_EXIST_SORT=0x01;
    const SORT_EXIST_DEFAULT=0x02;
    const SORT_EXIST_UNIQUE=0x04;
    const SORT_EXIST_UP=0x08;

    public static function SORT_TO_ARRAY($value){
        $value=(int)$value;
        $stringArray=array();
        if($value&& self::SORT_EXIST_SORT==1) $stringArray[]="SORT";
        if($value&& self::SORT_EXIST_DEFAULT==1) $stringArray[]="DEFAULT";
        if($value&& self::SORT_EXIST_UNIQUE==1) $stringArray[]="UNIQUE";
        if($value&& self::SORT_EXIST_UP==1) $stringArray[]="UP";
        return $stringArray;
    }

    // 记录了整体信息的数组
    protected $pageConfig;
    // 记录了每一个字段信息的数组
    protected $fieldArray;
    /** @var  DbData */
    protected $mode;

    /**
     * 增加一个要管理的字段
     * @param string $field 字段名
     * @param string $name 中文描述
     * @param string $type 值常量 TYPE_*
     * @param string $table 值常量 TABLE_*
     * @param string $modify 值常量 FORM_*
     * @param string|null $modify_group 所属的组,为空则不在任何组
     * @param string $search 值常量 FORM_*
     * @param int $sort 值常量 SEARCH_*
     * @return $this
     */
    protected function fieldAdd($field,$name,$type=self::TYPE_VARCHAR, $table=self::TABLE_NONE, $modify=self::FORM_NONE,
                                $modify_group=null, $search=self::FORM_NONE, $sort=self::SORT_NONE){
        $this->fieldArray[$field]=array(
            "name"=>$name, "type"=>$type, "table"=>$table, "modify"=>$modify,
            "modify_group"=>$modify_group,"search"=>$search, "sort"=>(int)$sort,
        );
        return $this;
    }
    protected function fieldModify($field,$config){
        $this->fieldArray[$field]=array_merge($this->fieldArray[$field], $config);
        return $this;
    }
    protected function pageConfig($key,$valueArray){
        $this->pageConfig[$key]=$valueArray;
        return $this;
    }
    protected function loadData($array){
        $fields=arrayUnsetAndReturn($array,"field");
        foreach($fields as $field){
            $this->fieldAdd($field['field'],$field['name'],$field['type'],$field['table'],
                $field['form'],$field['modify_group'],$field['search'],$field['sort']);
        }
        $this->pageConfig('modify_group_name',$array['modify_group_name']);
    }

    // 表格信息
    protected function getTableResult($condition=array(), $sortArray=array(), $page=1){
        $pageUrl=WebRouter::init()->getQuestion(array("condition"=>$condition,"sortArray"=>$sortArray,"page=#page#"));
        $pageManager=new Page($page,$pageUrl);
        $limit=$pageManager->getPageSize();
        $offset=$pageManager->getOffset();
        list($result['data'],$total)=$this->mode->getList(null,$condition,$sortArray,$limit,$offset);
        $pageManager->setTotal($total);
        $result['page']=$pageManager->getWidget();

        $result['showField']=array();
        $result['condition']=array();
        $result['sortArray']=array();
        $result['fieldArray']=$this->fieldArray;
        foreach($this->fieldArray as $field=>$config){
            // 处理
            if($config['table']!=self::TABLE_NONE){
                $result['showField'][]=$field;
            }
            if($config['search']!=self::FORM_NONE){
                $result['condition'][$field]=isset($condition[$field])?$condition[$field]:"";
            }
            if($config['sort']&self::SORT_EXIST_SORT>0){
                $result['sortArray'][$field]=isset($sortArray[$field])?$sortArray[$field]:true;
                if($config['sort']&self::SORT_EXIST_DEFAULT>0){
                    $result['defaultSort'][$field]=isset($config['sortDefault'])? $config['sortDefault']: true;
                }
            }
        }
        if(!isset($result['defaultSort'])) $result['defaultSort']=array();
        $result['url']['modify']="modify";
        $result['url']['delete']="delete";
        return $result;
    }

    // 修改、添加表单
    protected function getModifyResult($id=null){
        if(!empty($id)){
            $result['data']=$this->mode->getDetail($id);
            if(empty($result['data'])) return new Intent(404,"不存在该id");
        }

        $result['fieldArray']=$this->fieldArray;
        $result['pageConfig']=$this->pageConfig;
        $result['multipart']=false;

        foreach($this->fieldArray as $field=>$config){
            // 处理
            if($config['modify']!=self::FORM_NONE){
                $result['modifyGroup'][$config['modify_group']][]=$field;
            }
            if($config['modify']!=self::FORM_FILE){
                $result['multipart']=true;
            }
        }
        $result['url']['submitUrl']=WebRouter::init()->getAction("modifySubmit");
        return $result;
    }
}
class CodeCmsActivity extends CodeCms {
    /** @var  CmsView */
    protected $cms;
    function __construct() {
//        parent::__construct();
        $this->cms=CmsView::init();
    }

    protected function addControl($add=true, $list=true){
        if($list) $this->cms->controlWidget->addItem("list","fa-file","查看所有",null,"table");
        if($add) $this->cms->controlWidget->addItem("modify","fa-edit","添加",null,"modify");
    }

    function indexTask($condition=array(), $sortArray=array(), $page=1){
        return $this->tableTask($condition, $sortArray, $page);
    }

    function tableTask($condition=array(), $sortArray=array(), $page=1){
        $result=$this->getTableResult($condition, $sortArray, $page);
        $this->cms->codeTableScene($result);
    }
    function modifyTask($id=null){
        $result=$this->getModifyResult($id);
        $this->cms->codeFormScene($result);
    }

    function modifySubmitTask($data){
        $dm=$this->mode->update($data);
        if($dm instanceof DataMessage&& $dm->judgeState()){
            $url=WebRouter::init()->getAction("table");
            View::displayAsTips($url,$dm->getTitle());
        }else{
            View::displayDataMessage($dm);
        }
    }

    function deleteTask($id){
        $dm=$this->mode->delete($id);
        View::displayDataMessage($dm,true);
    }
}