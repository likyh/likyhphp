<?php
/** 自动CMS整体信息 */
class CodeCmsModule extends DbData {
    protected static $selfObjectArray;
    /** @return CodeCmsModule */
    public static function init($tableName = 'likyh_code_cms'){
        $tableName=!empty($tableName)&&validateVar($tableName)?$tableName:'likyh_code_cms';
        if(!isset(self::$selfObjectArray[$tableName])){
            self::$selfObjectArray[$tableName]=new CodeCmsModule($tableName);
        }
        return self::$selfObjectArray[$tableName];
    }

    //若无表建立通用表
    public function install(){
        $sql="CREATE TABLE `likyh_code_cms` (
              `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
              `modify_group_name` TEXT NOT NULL COMMENT '保存modify_group_name数组，json格式',
              `table_name` VARCHAR(255) NOT NULL COMMENT '',
              PRIMARY KEY (`id`)  COMMENT '存放CodeCms整体信息');";
        $this->db->sqlExec($sql);
        return true;
    }

    // 确定表名，确定字段数组
    protected $tableName;
    protected function __construct($tableName){
        parent::__construct();
        $this->tableName=$tableName;
        $this->tableInfo=array(
            "$tableName LCM"=>array("id","modify_group_name","table_name","*")
        );
        $this->referInfo=array();
    }
    // 获取列表
    public function getList($tableArray=array("LCM"), $condition, $sortArray=array(), $rows=null, $offset=0){
        $ifColumn=in_array("LCC",(array)$tableArray);
        $selectSql=$this->getFormatSelectSql(array("LCM"));
        $selectSql->addConditionArray($condition);
        $selectSql->setSortBy($sortArray);
        $sql=$selectSql->getSql($rows,$offset);
        list($list,$total)=$this->db->getList($sql);

        if($ifColumn){
            //TODO 表名只能更改，需要修改
            $codeColumnModule=CodeColumnModule::init();
            foreach($list as &$v){
                $v['field']=$codeColumnModule->getDetail($v['id']);
            }
        }

        return array($list,$total);
    }
    // 获取详细信息
    public function getDetail($ids){
        $selectSql=$this->getFormatSelectSql(array("LCM"));
        $selectSql->addConditionId($ids);
        $sql=$selectSql->getSql();
        $re=$this->db->getAll($sql);

        //TODO 表名只能更改，需要修改
        $codeColumnModule=CodeColumnModule::init();
        foreach($re as &$v){
            list($v['field'],$total)=$codeColumnModule->getList(null,array("likyh_code_id"=>$v['id']));
        }

        return is_array($ids)?$re:reset($re);
    }
    // 新建或者更新
    public function update($data){
        $re=parent::update($data);

        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"添加/修改成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"添加/修改失败",array("modifyLine"=>$re));
    }
    // 删除
    public function delete($id){
        $codeColumnModule=CodeColumnModule::init();
        $re=$this->db->delete($this->tableName,$id);
        $re+=$codeColumnModule->deleteByCodeId($id);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"删除成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"删除失败",array("modifyLine"=>$re));
    }
}
/** 自动CMS列信息 */
class CodeColumnModule extends DbData {
    protected static $selfObjectArray;
    /** @return CodeColumnModule */
    public static function init($tableName = 'likyh_code_column'){
        $tableName=!empty($tableName)&&validateVar($tableName)?$tableName:'likyh_code_column';
        if(!isset(self::$selfObjectArray[$tableName])){
            self::$selfObjectArray[$tableName]=new CodeColumnModule($tableName);
        }
        return self::$selfObjectArray[$tableName];
    }

    //若无表建立通用表
    public function install(){
        $sql="CREATE TABLE `likyh_code_column` (
          `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
          `likyh_code_id` INT NOT NULL COMMENT '',
          `field` VARCHAR(255) NOT NULL COMMENT '字段名',
          `name` VARCHAR(255) NULL COMMENT '字段中文描述',
          `type` ENUM('VARCHAR','PRIMARY','CUSTOM','TEXT','SET','ENUM','TIMESTAMP','TIME','DATE') NOT NULL DEFAULT 'VARCHAR' COMMENT '',
          `table` ENUM('NONE', 'STRING', 'CUSTOM', 'TEXT', 'MAP', 'DATETIME', 'TIME', 'DATE') NOT NULL DEFAULT 'NONE' COMMENT '',
          `form` ENUM('NONE', 'TEXT', 'HIDDEN', 'TEXTAREA', 'SELECT', 'DATETIME', 'TIME', 'DATE') NOT NULL DEFAULT 'NONE' COMMENT '',
          `modify_group` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '',
          `search` ENUM('NONE', 'TEXT', 'HIDDEN', 'TEXTAREA', 'SELECT', 'DATETIME', 'TIME', 'DATE') NOT NULL DEFAULT 'NONE' COMMENT '',
          `sort` SET('SORT', 'DEFAULT', 'UNIQUE', 'UP') NOT NULL COMMENT '',
          `map` TEXT NULL COMMENT '表示属性键值对的数组，json',
          `showFunction` VARCHAR(255) NULL COMMENT 'custom自定义显示函数，暂时不能在线编辑，需要自行手工在代码里面编写',
          PRIMARY KEY (`id`)  COMMENT '自动CMS 列信息');";
        $this->db->sqlExec($sql);
        return true;
    }

    // 确定表名，确定字段数组
    protected $tableName;
    protected function __construct($tableName){
        parent::__construct();
        $this->tableName=$tableName;
        $this->tableInfo=array(
            "$tableName LCC"=>array("id","likyh_code_id","field","name","type","table","form",
                "modify_group","search","sort","map","showFunction","*")
        );
        $this->referInfo=array();
    }
    // 获取列表
    public function getList($tableArray=array("LCC"), $condition, $sortArray=array(), $rows=null, $offset=0){
        $selectSql=$this->getFormatSelectSql(array("LCC"));
        $selectSql->addConditionArray($condition);
        $selectSql->setSortBy($sortArray);
        $sql=$selectSql->getSql($rows,$offset);
        return $this->db->getList($sql);
    }
    // 获取详细信息
    public function getDetail($ids){
        $selectSql=$this->getFormatSelectSql(array("LCC"));
        $selectSql->addConditionId($ids);
        $sql=$selectSql->getSql();
        $re=$this->db->getAll($sql);
        return is_array($ids)?$re:reset($re);
    }
    // 新建或者更新
    public function update($data){
        $re=parent::update($data);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"添加/修改成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"添加/修改失败",array("modifyLine"=>$re));
    }
    // 删除
    public function delete($id){
        $re=$this->db->delete($this->tableName,$id);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"删除成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"删除失败",array("modifyLine"=>$re));
    }
    // 批量删除 TODO 这是一个delete用到where语句的情况
    public function deleteByCodeId($code_id){
        $sql="delete from {$this->tableName} where likyh_code_id=?";
        return $this->db->sqlExec($sql,(int)$code_id);
    }

}