<?php
/**
 * 省份，城市，地区获取
 * Class AreaModule
 */
class AreaModule extends Data{
    protected $table;

    protected static $selfObjectArray;
    protected function __construct($table) {
        parent::__construct();
        $this->table=!empty($table)&&validateVar($table)? $table: 'likyh_area';
    }

    /**
     * 初始化函数，并返回当前类
     * @param string $table 表名
     * @return AreaModule
     */
    public static function init($table = 'likyh_photo'){
        $table=!empty($table)&& validateVar($table)? $table: 'likyh_area';
        if(!isset(self::$selfObjectArray[$table])){
            self::$selfObjectArray[$table]=new AreaModule($table);
        }
        return self::$selfObjectArray[$table];
    }

    // 获取某一个地点所有下一级内容
    function getSon($pid=0){
        $sql="SELECT `id`,`name`,`pid`,`level` FROM `{$this->table}` WHERE pid= ?";
        return $this->db->getAll($sql, $pid);
    }

    // 指定省所有市
    function getInfo($ids){
        $sql="SELECT `id`,`name`,`pid`,`level` FROM `{$this->table}` WHERE ".$this->db->getIdCondition($ids);
        $re =$this->db->getAll($sql);
        return is_array($ids)? $re: reset($re);
    }
    //由地址ID获取 上一级地址
    public function getFather($id){
        $data = $this->getInfo($id);
        $sql = "select * from `{$this->table}` where id=?";
        return $this->db->getOne($sql,$data['pid']);
    }

    // 根据一个地址，获取所有上一级内容
    function getFullAddress($id){
        $addressData=array();
        $re= $this->getInfo($id);
        while(isset($re)){
            $addressData[]= $re;
            $id=$re['id'];
            $re= $this->getFather($id);
        }
        return array_reverse($addressData);
    }
} 