<?php
/**
 * Class MenuModule
 * 数据表 id pid name level icon/url
 */
class MenuModule extends DbData {
    protected static $selfObjectArray;
    /** @return MenuModule */
    public static function init($tableName = 'likyh_menu'){
        $tableName=!empty($tableName)&&validateVar($tableName)?$tableName:'likyh_menu';
        if(!isset(self::$selfObjectArray[$tableName])){
            self::$selfObjectArray[$tableName]=new MenuModule($tableName);
        }
        return self::$selfObjectArray[$tableName];
    }

    //若无表建立通用表
    public function install(){
        $sql="CREATE TABLE IF NOT EXISTS `likyh_menu` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `pid` int(11) NOT NULL DEFAULT '0',
              `name` varchar(255) DEFAULT NULL,
              `point` INT NOT NULL DEFAULT '0',
              `level` varchar(255) DEFAULT NULL,
              `icon` varchar(255) DEFAULT NULL,
              `url` varchar(255) DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='';";
        $this->db->sqlExec($sql);
        return true;
    }

    // 确定表名，确定字段数组
    protected $tableName;
    protected function __construct($tableName){
        parent::__construct();
        $this->tableName=$tableName;
        $this->tableInfo=array(
            "$tableName LM"=>array("id","pid","name","point","level","icon","url","*")
        );
        $this->referInfo=array();
    }

    // 获取列表
    public function getList($tableArray=array("LM"), $condition, $sortArray=array(), $rows=null, $offset=0){
        $selectSql=$this->getFormatSelectSql(array("LM"));
//        $this->paramFilter($condition,array(),reset($this->tableInfo));
        $selectSql->addConditionArray($condition);
        $selectSql->setSortBy($sortArray);
        $sql=$selectSql->getSql($rows,$offset);
        return $this->db->getList($sql);
    }
    // 获取详细信息
    public function getDetail($ids){
        $selectSql=$this->getFormatSelectSql(array("LM"));
        $selectSql->addConditionId($ids);
        $sql=$selectSql->getSql();
        $re=$this->db->getAll($sql);
        return is_array($ids)?$re:reset($re);
    }
    // 检查是否存在
    public function checkExist($condition){
        $selectSql=$this->getFormatSelectSql(array("LM"));
//        $this->paramFilter($condition, array(), reset($this->tableInfo));
        $selectSql->addConditionArray($condition);
        $sql=$selectSql->getCountSql();
        return $this->db->getExist($sql);
    }

    // 所有分类，并且按照树组合好
    public function getMenuTree($pid=0){
        list($data,$total)=$this->getList(null,array("pid"=>(int)$pid));
        foreach(new ArrayIterator($data) as $k=>$v) {
            $data[$k]['children']=$this->getMenuTree((int)$v['id']);
        }
        return $data;
    }
    //获取id的所有子代id，包括自己id,一维数组显示
    function getPosterity($id){
        $targetArray=array($id);
        list($data,$total)=$this->getList(null,array("pid"=>(int)$id));
        foreach($data as $sonData){
            $targetArray=array_merge($targetArray,$this->getPosterity($sonData['id']));
        }
        return $targetArray;
    }
    // 获取子分类，如果本身就为叶节点，则获取父节点和兄弟节点
    public function getNotBottomSon($id){
        list($data,$total)=$this->getList(null,array("pid"=>(int)$id));
        if($total==0){
            $info=$this->getDetail($id);
            list($data,$total)=$this->getList(null,array("pid"=>(int)$info['pid']));
        }
        return array($data,$total);
    }
    // 获取ID所有的祖先节点(包括自身id)，越顶级的节点越前面
    public function getAncestor($id){
        $targetArray=array();
        do{
            $data=$this->getDetail($id);
            array_unshift($targetArray,$data);
            $id=$data['pid'];
        }while($id!=0);
        return array($targetArray,count($targetArray));
    }
    // 获取分类的所有底层分类，用于查询高等级id
    public function getBottom($id=0){
        list($data,$total)=$this->getList(null,array("pid"=>(int)$id));
        return $total>0? $this->getPosterity($id):array($id);
    }
    // 新建或者更新
    public function update($data){
        $re=parent::update($data);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"添加/修改成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"添加/修改失败",array("modifyLine"=>$re));
    }
    // 删除
    public function delete($id){
        $re=$this->db->delete($this->tableName,$id);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"删除成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"删除失败",array("modifyLine"=>$re));
    }
    //删除类别，包括子类别
    public function deleteRelated($pid){
        $re=0;
        list($data,$total)=$this->getList(null,array("pid"=>(int)$pid));
        if($total>0) $re+=$this->deleteRelated($pid);
        $re+=$this->db->delete($this->tableName,$pid);
        return $re;
    }
}
