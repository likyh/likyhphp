<?php

/**
 * Class CollectModule
 * 数据表 id user_id item_id create_time
 */
class CollectModule extends Data{
    protected $table;
    protected $user_id;
    protected $item_id;

    protected static $selfObjectArray;
    protected function __construct($table,$user_id,$item_id) {
        parent::__construct();
        $this->table=!empty($table)&&validateVar($table)?$table:'likyh_collect';
        $this->user_id=!empty($user_id)&&validateVar($user_id)?$user_id:'user_id';
        $this->item_id=!empty($item_id)&&validateVar($item_id)?$item_id:'item_id';
    }

    /**
     * 初始化函数，并返回当前类
     * @param string $table 表名
     * @param string $user_id 字段名
     * @param string $item_id 字段名
     * @return CollectModule
     */
    public static function init($table = 'likyh_collect',$user_id = 'user_id',$item_id = 'item_id'){
        if(!isset(self::$selfObjectArray[$table])){
            self::$selfObjectArray[$table]=new CollectModule($table,$user_id,$item_id);
        }
        return self::$selfObjectArray[$table];
    }
    // 若无表建立通用表
    public function install($comment = null){
        $comment=$this->db->quote($comment);
        $sql="CREATE TABLE IF NOT EXISTS `likyh_collect` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL ,
              `item_id` int(11) NOT NULL,
              `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT={$comment};";
        $this->db->sqlExec($sql);
        return true;
    }
    // select * ；收藏夹没有detail可言
    public function getList($user_id,$rows=30,$offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="SELECT SQL_CALC_FOUND_ROWS * FROM `{$this->table}` WHERE `{$this->user_id}`=? order by `create_time` DESC LIMIT $offset,$rows";
        return $this->db->getList($sql, $user_id);
    }

    public function add($user_id,$item_id){
        $data[$this->user_id]=(int)$user_id;
        $data[$this->item_id]=(int)$item_id;
        if($this->db->insert($this->table,$data) == 1){
            return (int)$this->db->insertId();
        }
        return -1;
    }
    // 移出
    public function remove($id){
        $id=(int)$id;
        return $this->db->delete($this->table,$id)==1;
    }

    // 检查是否收藏了
    function checkExist($user_id,$item_id){
        $sql="SELECT 1 FROM `{$this->table}` WHERE `{$this->user_id}`=? and `$this->item_id`=?";
        return $this->db->getExist($sql,$user_id,$item_id);
    }

    // item被收藏次数
    public function getNum($item_id){
        $sql="SELECT count(*) FROM `{$this->table}` WHERE `{$this->item_id}`=?";
        return $this->db->getValue($sql,$item_id);
    }
} 