<?php

/**
 * Class LabelModule
 * 数据表 id name
 */
class LabelModule extends Data{
    protected $table;

    protected static $selfObjectArray;
    protected function __construct($table) {
        parent::__construct();
        $this->table=!empty($table)&&validateVar($table)?$table:'likyh_label';
    }

    /**
     * 初始化函数，并返回当前类
     * @param string $table 表名
     * @return LabelModule
     */
    public static function init($table = 'likyh_label'){
        if(!isset(self::$selfObjectArray[$table])){
            self::$selfObjectArray[$table]=new LabelModule($table);
        }
        return self::$selfObjectArray[$table];
    }
    // 若无表建立通用表
    public function install($comment = null){
        $comment=$this->db->quote($comment);
        $sql="CREATE TABLE IF NOT EXISTS `likyh_label` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` int(11) NOT NULL ,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT={$comment};";
        $this->db->sqlExec($sql);
        return true;
    }
    // $ids格式为 1 或者 1,2
    public function getDetail($ids){
        $sql="select * from `{$this->table}` where ".$this->db->getIdCondition($ids);
        $data=$this->db->getAll($sql, $ids);
        return is_array($ids)? $data: reset($data);
    }
    //搜出所有标签
    public function getList($rows=30,$offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="SELECT * FROM `{$this->table}` limit $offset,$rows";
        return $this->db->getList($sql);
    }
    //搜出商品具有的标签
    public function getListByIds($ids){
        if(empty($ids)){
            return array();
        }
        $sql="select * from `{$this->table}` where FIND_IN_SET(id,?)";
        return $this->db->getAll($sql, $ids);
    }

    public function add($name){
        $data['name']=$name;
        if($this->db->insert($this->table,$data) == 1){
            return (int)$this->db->insertId();
        }
        return -1;
    }
    public function checkExist($name){
        $sql="SELECT 1 FROM `{$this->table}` WHERE `name`=?";
        return $this->db->getExist($sql,$name);
    }
    public function delete($id){
        $id=(int)$id;
        return $this->db->delete($this->table,$id)==1;
    }
    public function modify($id,$name){
        if($this->checkExist($name)){
            return "该标签名已经存在了，请勿重复定义";
        }
        $id=(int)$id;
        $data['name']=$name;
        if($this->db->update($this->table,$id,$data)){
            return 'success';
        }else{
            return "修改失败";
        }
    }
} 