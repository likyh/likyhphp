<?php
// 站点键值对信息
class SiteInfoModule extends DbData {
    protected $tableName;
    protected static $selfObjectArray;
    /** @return SiteInfoModule */
    public static function init($tableName = 'likyh_site_info'){
        $tableName=!empty($tableName)&&validateVar($tableName)?$tableName:'likyh_site_info';
        if(!isset(self::$selfObjectArray[$tableName])){
            self::$selfObjectArray[$tableName]=new SiteInfoModule($tableName);
        }
        return self::$selfObjectArray[$tableName];
    }

    //若无表建立通用表
    public function install(){
        $sql="CREATE TABLE IF NOT EXISTS `likyh_site_info` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `title` varchar(255) NOT NULL,
              `content` TEXT NOT NULL,
              `picture` varchar(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='记录了站点键值对信息';
        ALTER TABLE `likyh_site_info` ADD UNIQUE(`title`);";
        $this->db->sqlExec($sql);
        return true;
    }

    protected function __construct($tableName) {
        parent::__construct();
        $this->tableName=$tableName;
        $this->tableInfo=array(
            "$tableName LSI"=>array("id","title","content","picture","*")
        );
        $this->referInfo=array();
    }

    /**
     * 根据类型进行搜索，模糊搜索则返回一个二维数组，否则返回一维数组或者null
     * @param $type
     * @param bool $vague 是否启用模糊搜索
     * @return array
     */
    public function getDetailByTitle($title,$vague=false){
        $selectSql=$this->getFormatSelectSql(array("LSI"));
        if($vague){
            $field=$selectSql->getFieldFullName("title");
            $title= $selectSql->db->quote("%$title%");
            $selectSql->addConditionSql("$field like $title");
            $sql=$selectSql->getSql();
        }else{
            $selectSql->addCondition("title",$title);
            $sql=$selectSql->getSql(1);
        }
        $re=$this->db->getAll($sql);
        return $vague?$re:reset($re);
    }

    // 获取列表
    public function getList($tableArray=array("LSI"), $condition, $sortArray=array(), $rows=null, $offset=0){
        $selectSql=$this->getFormatSelectSql(array("LSI"));
        $selectSql->addConditionArray($condition);
        $selectSql->setSortBy($sortArray);
        $sql=$selectSql->getSql($rows,$offset);
        return $this->db->getList($sql);
    }
    // 获取详细信息
    public function getDetail($ids){
        $selectSql=$this->getFormatSelectSql(array("LSI"));
        $selectSql->addConditionId($ids);
        $sql=$selectSql->getSql();
        $re=$this->db->getAll($sql);
        return is_array($ids)?$re:reset($re);
    }
    // 新建或者更新
    public function update($data){
        $re=parent::update($data);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"添加/修改成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"添加/修改失败",array("modifyLine"=>$re));
    }
    // 删除
    public function delete($id){
        $re=$this->db->delete($this->tableName,$id);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"删除成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"删除失败",array("modifyLine"=>$re));
    }
} 