<?php
/** 文章类 */
class ArticleModule extends DbData {
    protected static $selfObjectArray;
    /** @return ArticleModule */
    public static function init($tableName = 'likyh_article'){
        $className=get_called_class();
        $tableName=!empty($tableName)&&validateVar($tableName)?$tableName:'likyh_article';
        if(!isset(self::$selfObjectArray[$tableName])){
            self::$selfObjectArray[$tableName]=new $className($tableName);
        }
        return self::$selfObjectArray[$tableName];
    }

    //若无表建立通用表
    public function install(){
        $sql="CREATE TABLE likyh_article(
            id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            `type` VARCHAR(255),
            `title` VARCHAR(255) NOT NULL,
            `content` LONGTEXT,
            `owner` int(11),
            `remark` VARCHAR(255),
            `publish_time` DATETIME,
            `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='记录所有文章的表';
        CREATE UNIQUE INDEX remark_UNIQUE ON likyh_article (remark);";
        $this->db->sqlExec($sql);
        return true;
    }

    // 确定表名，确定字段数组
    protected $tableName;
    protected function __construct($tableName){
        parent::__construct();
        $this->tableName=$tableName;
        $this->tableInfo=array(
            "$tableName LA"=>array("id","type","title","content","owner","remark","publish_time","create_time","*")
        );
        $this->referInfo=array();
    }
    // 获取列表
    public function getList($tableArray=array("LA"), $condition, $sortArray=array(), $rows=null, $offset=0){
        $selectSql=$this->getFormatSelectSql(array("LA"));
        arrayDealArray($condition,function($key,$value)use($selectSql){
            if(!empty($value)) $selectSql->addCondition($key,"%$value%","like");
        },array("content","title",));
        $selectSql->addConditionArray($condition);
        $selectSql->setSortBy($sortArray);
        $sql=$selectSql->getSql($rows,$offset);
        return $this->db->getList($sql);
    }
    // 获取详细信息
    public function getDetail($ids){
        $selectSql=$this->getFormatSelectSql(array("LA"));
        $selectSql->addConditionId($ids);
        $sql=$selectSql->getSql();
        $re=$this->db->getAll($sql);
        return is_array($ids)?$re:reset($re);
    }
    // 新建或者更新
    public function update($data){
        $re=parent::update($data);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"添加/修改成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"添加/修改失败",array("modifyLine"=>$re));
    }
    // 删除
    public function delete($id){
        $re=$this->db->delete($this->tableName,$id);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"删除成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"删除失败",array("modifyLine"=>$re));
    }
}