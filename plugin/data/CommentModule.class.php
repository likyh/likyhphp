<?php

/**
 * Class CommentModule
 * 数据表 id user_id item_id comment create_time score pic_ids reply reply_time
 */
class CommentModule extends Data{
    protected $table;
    protected $user_id;
    protected $item_id;

    protected static $selfObjectArray;
    protected function __construct($table,$user_id,$item_id) {
        parent::__construct();
        $this->table=!empty($table)&&validateVar($table)?$table:'likyh_comment';
        $this->user_id=!empty($user_id)&&validateVar($user_id)?$user_id:'user_id';
        $this->item_id=!empty($item_id)&&validateVar($item_id)?$item_id:'item_id';
    }

    /**
     * 初始化函数，并返回当前类
     * @param string $table 表名
     * @param string $user_id 字段名
     * @param string $item_id 字段名
     * @return CommentModule
     */
    public static function init($table = 'likyh_comment',$user_id = 'user_id',$item_id = 'item_id'){
        if(!isset(self::$selfObjectArray[$table])){
            self::$selfObjectArray[$table]=new CommentModule($table,$user_id,$item_id);
        }
        return self::$selfObjectArray[$table];
    }

    //若无表建立通用表
    public function install($comment = null){
        $comment=$this->db->quote($comment);
        $sql="CREATE TABLE IF NOT EXISTS `likyh_comment` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `user_id` int(11) NOT NULL ,
              `item_id` int(11) NOT NULL,
              `comment` TEXT NULL,
              `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `score` int(11) NOT NULL DEFAULT '5',
              `pic_ids` VARCHAR(255) NULL,
              `reply` TEXT NULL,
              `reply_time` TIMESTAMP NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT={$comment};";
        $this->db->sqlExec($sql);
        return true;
    }
    //list+total
    public function getList($user_ids=null,$item_ids=null,$rows=30,$offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $condition='';
        if(!empty($user_ids)){
            $condition.=" AND ".$this->db->getIdCondition($user_ids,$this->user_id);
        }
        if(!empty($item_ids)){
            $condition.=" AND ".$this->db->getIdCondition($item_ids,$this->item_id);
        }
        $sql="SELECT SQL_CALC_FOUND_ROWS * FROM `{$this->table}` WHERE 1 $condition
        order by `create_time` DESC limit $offset,$rows";
        return $this->db->getList($sql);
    }
    function getDetail($id){
        $sql="SELECT * FROM `{$this->table}` WHERE `id`=?";
        return $this->db->getOne($sql,$id);
    }
    public function add($user_id,$item_id,$comment,$score = 5,$pic_ids = null){
        $data[$this->user_id]=$user_id;
        $data[$this->item_id]=$item_id;
        $data['comment']=$comment;
        $data['score']=$score;
        $data['pic_ids']=$pic_ids;
        if($this->db->insert($this->table,$data) == 1){
            return (int)$this->db->insertId();
        }
        return -1;
    }
    public function delete($id){
        $id=(int)$id;
        return $this->db->delete($this->table,$id)==1;
    }
    //检查是否已评价
    public function checkExist($user_id,$item_id){
        $sql="SELECT 1 FROM `{$this->table}` WHERE `{$this->user_id}`=? and `{$this->item_id}`=?";
        return $this->db->getExist($sql,$user_id,$item_id);
    }
    function addReply($id,$reply){
        $id=(int)$id;
        $data['reply']=$reply;
        $data['reply_time']=date('Y-m-d H:i:s',time());
        return $this->db->update($this->table,$id,$data);
    }
    //用两个id搜。。
    function getDetailByTowIds($user_id,$item_id){
        $sql="SELECT * FROM `{$this->table}` WHERE `{$this->user_id}`=? and `{$this->item_id}`=?";
        return $this->db->getOne($sql,$user_id,$item_id);
    }
} 