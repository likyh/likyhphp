<?php
// 用户通用表
class UserModule extends DbData {
    protected static $selfObjectArray;
    /** @return UserModule */
    public static function init($tableName = 'likyh_user',$infoName = 'likyh_user_info'){
        $tableName=!empty($tableName)&&validateVar($tableName)?$tableName:'likyh_article';
        $infoName=!empty($infoName)&&validateVar($infoName)?$infoName:'likyh_user_info';
        $key=$tableName.$infoName;
        if(!isset(self::$selfObjectArray[$key])){
            self::$selfObjectArray[$key]=new UserModule($tableName,$infoName);
        }
        return self::$selfObjectArray[$key];
    }
    //无表时建立通用表
    function install(){
        $sql="CREATE TABLE IF NOT EXISTS `likyh_user` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `user` VARCHAR(255) NULL,
                `tel` VARCHAR(255) NULL,
                `email` VARCHAR(255) NULL,
                `nickname` VARCHAR(255) NULL,
                `wechat_open_id` VARCHAR(255) NULL,
                `code` VARCHAR(255) NULL,
                `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `last_time` DATETIME NULL,
                `type` TINYINT(4) DEFAULT 1,
                `enable` TINYINT(4) DEFAULT 1,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户表';
            CREATE UNIQUE INDEX user_UNIQUE ON likyh_user (`user`);
            CREATE UNIQUE INDEX tel_UNIQUE ON likyh_user (`tel`);
            CREATE UNIQUE INDEX email_UNIQUE ON likyh_user (`email`);
            CREATE UNIQUE INDEX wechat_open_id_UNIQUE ON likyh_user (`wechat_open_id`);
            CREATE TABLE IF NOT EXISTS likyh_user_info (
                `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                `user_id` INT NOT NULL,
                `real_name` VARCHAR(255),
                `birth` DATE,
                `idnum` VARCHAR(255),
                `point` INT DEFAULT 0 NOT NULL,
                `money` DECIMAL(11,2) DEFAULT 0.00 NOT NULL
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='用户信息表';";
        $this->db->sqlExec($sql);
        return true;
    }

    // 确定表名，确定字段数组
    protected $tableName;
    protected $infoName;
    protected function __construct($tableName,$infoName){
        parent::__construct();
        SimpleSession::init();
        $this->tableName=$tableName;
        $this->infoName=$infoName;
        $this->tableInfo=array(
            // TODO 加上了Code 不知道会造成什么不好的影响
            "$tableName LU"=>array("id","user","code","tel","email","nickname","wechat_open_id","create_time","last_time","type","enable"),
            "$infoName LUI"=>array("id"=>"user_info_id","user_id","real_name","birth","idnum","point","money","*"),
        );
        $this->referInfo=array(
            "id"=>"user_id"
        );
    }

    // 根据模糊的用户名返回用户信息，如果不存在返回null
    function userMatch($user){
        $selectSql=$this->getFormatSelectSql(array("LU","LUI"));
        $userField=$selectSql->getFieldFullName("user");
        $telField=$selectSql->getFieldFullName("tel");
        $emailField=$selectSql->getFieldFullName("email");
        $user=$selectSql->db->quote($user);
        $selectSql->addConditionSql("($userField=$user or $telField=$user or $emailField=$user)");
        $sql=$selectSql->getSql(1,0);
        return $this->db->getOne($sql);
    }
    // 密码不需要使用加密过的，明文传递来即可
    protected function codeMatch($user,$pass){
        $sql="select 1 from {$this->tableName} where user=? and code=?";
        $code=getPassWord($user,$pass);
        return $this->db->getExist($sql,$user,$code);
    }

    // 用户登录
    function login($user,$pass){
        if(empty($user)|| empty($pass)) return new DataMessage(DataMessage::STATE_WARRING,'用户名或者密码为空');
        $userData=$this->userMatch($user);
        if(empty($userData)) return new DataMessage(DataMessage::STATE_ERROR,'用户名不存在');
        if(!$this->codeMatch($userData['user'],$pass)){
            return new DataMessage(DataMessage::STATE_ERROR,'登录失败,密码错误');
        }
        if($userData['enable']){
            $data['id']=$userData['id'];
            $data['last_time']=date("Y-m-d H:i:s");
            parent::update($data);
            $_SESSION[CONFIG_SITE_NAME][$this->tableName]['login_user_id']=true;
            $_SESSION[CONFIG_SITE_NAME][$this->tableName]['info']=$userData;
            return new DataMessage(DataMessage::STATE_SUCCESS,'登录成功');
        }else{
            return new DataMessage(DataMessage::STATE_ERROR,'登录失败,该用户名暂未审核，请联系客服');
        }
    }
    public function getLoginId(){
        return isset($_SESSION[CONFIG_SITE_NAME][$this->tableName]['login_user_id'])?(int)$_SESSION[CONFIG_SITE_NAME][$this->tableName]['info']['user_id']:null;
    }
    function getLoginInfo(){
        $id=$this->getLoginId();
        return !empty($id)? $this->getDetail($id): null;
    }
    public function loginout(){
        unset($_SESSION[CONFIG_SITE_NAME][$this->tableName]);
    }

    // 简单注册
    function register($data){
        if(empty($data['user'])|| empty($data['code'])) return new DataMessage(DataMessage::STATE_WARRING,'用户名或者密码为空');

        $userData=$this->userMatch($data['user']);
        if(!empty($userData)) return new DataMessage(DataMessage::STATE_ERROR,'用户名已存在');

        $data['code'] = getPassWord($data['user'],$data['code']);
        return parent::update($data)>0? new DataMessage(DataMessage::STATE_SUCCESS,"注册成功"):
            new DataMessage(DataMessage::STATE_ERROR,"注册失败");
    }

    // 用户修改密码 (需要原始密码)
    function passwordChange($user,$old,$new){
        if(empty($user)|| empty($old)|| empty($new)) return new DataMessage(DataMessage::STATE_WARRING,'用户名或者密码为空');
        if($old==$new) return new DataMessage(DataMessage::STATE_WARRING,'输入的新旧密码相同');
        $userData=$this->userMatch($user);
        if(empty($userData)) return new DataMessage(DataMessage::STATE_ERROR,'用户名不存在');

        if(!$this->codeMatch($userData['user'],$old)){
            return new DataMessage(DataMessage::STATE_ERROR,'修改失败','旧密码错误');
        }

        $data['id']=$userData['id'];
        $data['code']=getPassWord($userData['user'],$new);
        $re=parent::update($data);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"修改成功"):
            new DataMessage(DataMessage::STATE_ERROR,"修改失败");
    }
    // 不需要原来的密码直接重置密码，如果$pass不传递，则自动生成一个密码并返回
    function passwordReset($user,$pass=null){
        if(empty($user)) return new DataMessage(DataMessage::STATE_WARRING,'用户名为空');
        if(empty($pass)){
            $pass=substr(md5(date("Y-m-d H:i:s").rand(10000,99999)),0,8);
        }
        $userData=$this->userMatch($user);
        if(empty($userData)) return new DataMessage(DataMessage::STATE_ERROR,'用户名不存在');

        $code=getPassWord($userData['user'],$pass);

        $data['id']=$userData['id'];
        $data['code']=$code;
        $re=parent::update($data);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"修改成功",array("pass"=>$pass)):
            new DataMessage(DataMessage::STATE_ERROR,"修改失败",array("pass"=>$pass));
    }

    // 检查手机格式正确否
    function checkTelFormatValid($tel){
        return preg_match('/^1\d{10}$/',$tel);
    }
    // 检查邮箱格式正确否
    function checkEmailFormatValid($email){
        return preg_match('/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/',$email);
    }
    // 修改enable
    public function changeEnable($user_id,$enable=0){
        $data['id']=$user_id;
        $data['enable']=$enable;
        return $this->update($data);
    }

    // 获取列表
    public function getList($tableArray=array("LU","LUI"), $condition, $sortArray=array(), $rows=null, $offset=0){
        $selectSql=$this->getFormatSelectSql(array("LU","LUI"));
        $this->paramFilter($condition,array(),reset($this->tableInfo));
        $selectSql->addConditionArray($condition);
        $selectSql->setSortBy($sortArray);
        $sql=$selectSql->getSql($rows,$offset);
        return $this->db->getList($sql);
    }
    // 获取详细信息
    public function getDetail($ids){
        $selectSql=$this->getFormatSelectSql(array("LU","LUI"));
        $selectSql->addConditionId($ids);
        $sql=$selectSql->getSql();
        $re=$this->db->getAll($sql);
        return is_array($ids)?$re:reset($re);
    }
    // 新建或者更新
    public function update($data){
        $re=parent::update($data);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"添加/修改成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"添加/修改失败",array("modifyLine"=>$re));
    }
    // 删除
    public function delete($id){
        $data=$this->getDetail($id);
        $re=parent::delete($data);
        return $re>0? new DataMessage(DataMessage::STATE_SUCCESS,"删除成功",array("modifyLine"=>$re)):
            new DataMessage(DataMessage::STATE_ERROR,"删除失败",array("modifyLine"=>$re));
    }
}