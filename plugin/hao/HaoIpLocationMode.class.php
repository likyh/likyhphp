<?php

/**
 * haoService IP定位
 * Class HaoIpLocationMode
 */
class HaoIpLocationMode extends Data{
    /** @return HaoIpLocationMode */
    public static function init() {
        return parent::init();
    }

    protected $appkey;
    protected $config;
    protected function onStart() {
        $this->config=getConfig("hao","ipLocation",true);
        $this->appkey=$this->config["appkey"];
    }

    /**
     * @param string $ip
     * @return bool|mixed
     */
    function getLocation($ip){
        $url=$this->config['url'];
        $param=array(
            "ip" =>$ip,
            "key" =>$this->appkey
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }
} 