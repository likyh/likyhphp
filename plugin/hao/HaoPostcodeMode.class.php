<?php

/**
 * haoService 邮编查询
 * Class HaoPostcodeMode
 */
class HaoPostcodeMode extends Data{
    /** @return HaoPostcodeMode */
    public static function init() {
        return parent::init();
    }

    protected $appkey;
    protected $config;
    protected function onStart() {
        $this->config=getConfig("hao","postcode",true);
        $this->appkey=$this->config["appkey"];
    }

    /**
     * 邮编查询地名,一般返回多个
     * @param string $postcode
     * @param int $page
     * @param int $pagesize
     * @return bool|mixed
     */
    function searchArea($postcode,$page=1,$pagesize=20){
        $url=$this->config['search'];
        $param=array(
            "key" => $this->appkey,
            "postcode" => $postcode,
            "page" => $page,
            "pagesize" =>$pagesize
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }

    /**
     * 地名查询邮编
     * @param int $pid
     * @param int $cid
     * @param int $did
     * @param null $q
     * @return bool|mixed
     */
    function searchPostcode($pid,$cid,$did,$q=null){
        $url=$this->config['search'];
        $param=array(
            "key" => $this->appkey,
            "pid" => $pid,
            "cid" => $cid,
            "did" => $did,
            "q" =>$q
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }
} 