<?php
/**
 * haoService 天气
 * Class HaoWeatherMode
 */
class HaoWeatherMode extends Data{
    /** @return HaoWeatherMode */
    public static function init() {
        return parent::init();
    }

    protected $appkey;
    protected $config;
    protected function onStart() {
        $this->config=getConfig("hao","weather",true);
        $this->appkey=$this->config["appkey"];
    }

    /**
     * 根据城市名查询天气
     * @param string $city
     * @return bool|mixed
     */
    public function getBase($city){
        $url=$this->config["index"];
        $param=array(
            "cityname"=>$city,
            "dtype" => "json",
            "key" => $this->appkey
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }

    /**
     * 天气种类及标识列表
     * @return bool|mixed
     */
    public function getUni(){
        $url=$this->config["uni"];
        $param=array(
            "dtype" => "json",
            "key" => $this->appkey
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }

    /**
     * 根据IP查询天气
     * @param string $ip
     * @return bool|mixed
     */
    public function getIp($ip){
        $url=$this->config['ip'];
        $param=array(
            "ip" => $ip,
            "dtype" => "json",
            "key" => $this->appkey
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }

    /**
     * 根据GPS坐标查询天气
     * @param string $lon
     * @param string $lat
     * @return bool|mixed
     */
    public function getGps($lon,$lat){
        $url=$this->config['gps'];
        $param=array(
            "lon" => $lon,
            "lat" => $lat,
            "dtype" =>"json",
            "key" => $this->appkey
        );
        $dataStr=Request::get($url, $param);
        return $dataStr!==false? json_decode($dataStr,true): false;
    }
} 