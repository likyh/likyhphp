<?php
class TestifySqlDB extends SqlDB {
    protected static $selfObject;
    function __construct($host=null,$port=null,$user=null,$pass=null,$dbname=null) {
        parent::__construct($host, $port, $user, $pass, $dbname);
    }
    // 获取数据库操作类的引用对象
    static public function init($host=null,$port=null,$user=null,$pass=null,$dbname=null){
        if(!isset(self::$selfObject)){
            self::$selfObject= new TestifySqlDB($host,$port,$user,$pass,$dbname);
        }
        return self::$selfObject;
    }

    public static $modifyCounter=0;
    public static $modifyCache=array();
    public function sqlExec($sql){
        self::$modifyCache[self::$modifyCounter]['sql']=$sql;
        return true;
    }

    function insert($tableName, $data, $blankRetain = false){
        self::$modifyCache[self::$modifyCounter]['tableName']=$tableName;
        self::$modifyCache[self::$modifyCounter]['data']=$data;
        self::$modifyCounter++;
        return parent::insert($tableName, $data, $blankRetain);
    }

    function update($tableName, $id, $data, $blankRetain = false){
        self::$modifyCache[self::$modifyCounter]['tableName']=$tableName;
        self::$modifyCache[self::$modifyCounter]['id']=$id;
        self::$modifyCache[self::$modifyCounter]['data']=$data;
        self::$modifyCounter++;
        return parent::update($tableName, $id, $data, $blankRetain);
    }

    function delete($tableName, $id){
        self::$modifyCache[self::$modifyCounter]['tableName']=$tableName;
        self::$modifyCache[self::$modifyCounter]['id']=$id;
        self::$modifyCounter++;
        return parent::delete($tableName, $id);
    }

    function insertId() {
        return self::$modifyCounter-1+10000;
    }
}
class Testify {
    protected static $selfObject;
    /** @return Testify */
    public static function init(){
        if(!isset(self::$selfObject)){
            self::$selfObject=new Testify();
        }
        return self::$selfObject;
    }

    /** @var  array(TestifyCase) 所有测试用例 */
    protected $testifyCase;

    // 开始测试
    public function test(TestifyCase $testifyCase, $sqlOnlyRead=true, $viewCache=true){
        if($sqlOnlyRead) Data::$staticDb=TestifySqlDB::init();
        if($viewCache) View::$debug=true;
        $testifyCase->run();
        $this->testifyCase[]=$testifyCase;
        if($viewCache) View::$debug=false;
        if($sqlOnlyRead) Data::$staticDb=SqlDB::init();
    }

    public function report(){
        $result=array();
        $result['passCount']=0;
        $result['failCount']=0;
        foreach($this->testifyCase as $case){
            // 记录每一个测试用例的情况 （TestifyCase）
            if(!$case instanceof TestifyCase) break;
            $caseTmp=array();
            $caseTmp['name']="#".$case->name;
            $caseTmp['passCount']=0;
            $caseTmp['failCount']=0;
            foreach($case->testData as $k=>$data){
                // 记录每一组数据的情况
                $data['name']="#".($data['name']?:($k+1));
                foreach($data['column'] as $kc=>&$column){
                    // 记录每一列（每一个compare成功失败）
                    $column['source']=$this->getFileLine($column['file'],$column['line']);
                }
                $caseTmp['data'][]=$data;
                $caseTmp['passCount']+=$data['pass'];
                $caseTmp['failCount']+=!$data['pass'];
            }
            $result['case'][]=$caseTmp;
            $result['passCount']+=$caseTmp['passCount'];
            $result['failCount']+=$caseTmp['failCount'];
        }
        return $result;
    }

    public function reportCgi(){
        foreach($this->testifyCase as $case){
            if(!$case instanceof TestifyCase) break;
            echo "TestifyCase #{$case->name}: \n";
            foreach($case->testData as $k=>$data){
                if($data['pass']){
                    echo "  Data #".($data['name']?:$k+1). ": Pass \n";
                }else{
                    echo "  Data #".($data['name']?:$k+1). ": Fail \n";
                    foreach($data['column'] as $kc=>$column){
                        echo "    Column #".($kc+1).": ".($column['pass']?"Pass":"Fail")." \n";
                    }
                }
            }
        }
    }

    protected $fileCache=array();
    public function getFileLine($file, $line){
        if (!array_key_exists($file, $this->fileCache)) {
            $this->fileCache[$file] = file($file);
        }

        return trim($this->fileCache[$file][$line-1]);
    }
}
class TestifyCase {
    /** @var  Closure */
    protected $testCase;
    public $name;
    protected $paramName;
    protected $pass=0;
    protected $fail=0;
    /** @var  array(array()) 记录待测试的数据 */
    public $testData;

    /**
     * 创建测试用例
     * @param string $name 测试用例名称
     */
    function __construct($name){
        $this->name = $name;
    }
    // 设置负责测试的函数
    public function testCase(Closure $testCase){
        $this->testCase = $testCase;

        $refFunc=new ReflectionFunction($testCase);
        $paramName=array_map(function($v){return $v->name;}, $refFunc->getParameters());

        assert(array_shift($paramName)=="assert");
        $this->paramName = $paramName;
        return $this;
    }

    // 添加一组测试数据
    public function addData($assert, $data, $name=null){
        $this->testData[]=array(
            "assert" => $assert,
            "data" => $data,
            "name" => $name,
        );
        return $this;
    }
    // json格式的文件，整个数组和$this->testData同样的格式
    public function addDataByFile($filename){
        $data=json(SimpleFile::read($filename));
        $this->testData= array_merge($this->testData, $data);
        return $this;
    }
    // 数组和$this->testData同样的格式
    public function addDataArray($array){
        $this->testData= array_merge($this->testData, $array);
        return $this;
    }

    // 记录断言结果
    protected $dataXResult;
    public function compare($result){
        $tmp['pass']= (bool)$result;
        $bt = debug_backtrace();
        $tmp['line']= $bt[0]['line'];
        $tmp['file']= $bt[0]['file'];
        $this->dataXResult[]= $tmp;
    }
    protected function getCompareInfo(){
        $dataResult=true;
        $dataTmp=$this->dataXResult;
        foreach($this->dataXResult as $v){
            $dataResult= $dataResult&& $v['pass'];
        }
        $this->dataXResult= array();
        return array($dataResult, $dataTmp);
    }

    // 在运行前后需要设置的内容
    protected $before = null;
    protected $after = null;
    protected $beforeEach = null;
    protected $afterEach = null;
    public function before(Closure $callback){
        $this->before = $callback;
        return $this;
    }
    public function after(Closure $callback){
        $this->after = $callback;
        return $this;
    }
    public function beforeEach(Closure $callback){
        $this->beforeEach = $callback;
        return $this;
    }
    public function afterEach(Closure $callback){
        $this->afterEach = $callback;
        return $this;
    }

    // 根据测试数据处理测试用例
    public function run(){
        if (is_callable($this->before)) {
            call_user_func($this->before, $this);
        }

        foreach($this->testData as $k=>&$v){
            if (is_callable($this->beforeEach)) {
                call_user_func($this->beforeEach, $this);
            }

            // 开始测试，其他的都是测试前后的设计
            $param=$v['data'];
            array_unshift($param, $v['assert']);
            call_user_func_array($this->testCase, $param);

            // 记录结果
            list($v['pass'],$v['column'])=$this->getCompareInfo();
            if($v['pass']){
                $this->pass++;
            }else{
                $this->fail++;
            }

            if (is_callable($this->afterEach)) {
                call_user_func($this->afterEach, $this);
            }
        }

        if (is_callable($this->after)) {
            call_user_func($this->after, $this);
        }
        return $this;
    }
}